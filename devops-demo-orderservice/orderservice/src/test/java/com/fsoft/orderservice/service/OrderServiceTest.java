package com.fsoft.orderservice.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@PrepareForTest(OrderServiceImpl.class)
public class OrderServiceTest {

    private OrderServiceImpl orderService = new OrderServiceImpl();

    @Value("${serviceEndpoint.productService:abc}")
    private String productEndpoint;

    @Test
    public void duplicateOrder_Success() {
        Assert.assertNotNull(orderService.duplicateOrder());
    }

    @Test
    public void findOrder_Success() {
        Assert.assertNotNull(OrderServiceImpl.findOrder());
    }
}
