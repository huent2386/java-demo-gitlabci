package com.fsoft.orderservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long productId;

    private String productCode;

    private String productName;

    private Integer quantity;

    private String description;

    @ManyToOne
    @JoinColumn
    @JsonIgnore
    private CustomerOrder customerOrder;

}
