package com.fsoft.orderservice.service;

import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.orderservice.entity.CustomerOrder;
import com.fsoft.orderservice.model.Product;
import com.fsoft.orderservice.model.ProductResponse;
import com.fsoft.orderservice.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@Service
public class OrderServiceImpl extends BaseService implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Value("${serviceEndpoint.productService}")
    private String productEndpoint;

    @Override
    @Transactional
    public CustomerOrder createOrder(CustomerOrder request) {

        ResponseEntity<ProductResponse> response = null;
        request.getOrderDetails()
                .stream()
                .map(item -> {
                    item.setCustomerOrder(request);
                    return item;
                })
                .map(item -> Product.builder()
                                .code(item.getProductCode())
                                .volume(new BigDecimal(item.getQuantity()))
                                .build())
                .forEach(item -> callService(item));


        return orderRepository.save(request);
    }

    public ProductResponse callService(Product product) {
        String deductEndpoint = String.format("%s/products/deduct", productEndpoint);
        ResponseEntity<ProductResponse> response;
        try {

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<Product> entity = new HttpEntity<Product>(product,headers);

            response = restTemplate.exchange(deductEndpoint, HttpMethod.POST, entity,
                    new ParameterizedTypeReference<ProductResponse>() {
            });

            if(response == null || response.getBody() == null) {
			    throw new CoreServerRuntimeException("-100", "Response null");
            }

            if (!response.getBody().getCode().equals("200")) {
                throw new CoreServerRuntimeException(response.getBody().getCode(), response.getBody().getMessage());
            }

            return response.getBody();
        }catch (Exception e) {
            throw e;
        }
    }

    @Override
    public CustomerOrder findOrder(Long orderId) {
        return orderRepository.findById(orderId).orElse(null);
    }

    @Override
    public List<CustomerOrder> findAll() {
        return orderRepository.findAll();
    }
    @Override
    public CustomerOrder duplicateOrder() {
        return new CustomerOrder();
    }

    public static CustomerOrder findOrder() {
        return new CustomerOrder();
    }

}
