package com.fsoft.orderservice.controller;

import com.fsoft.common.models.ResponseData;
import com.fsoft.orderservice.entity.CustomerOrder;
import com.fsoft.orderservice.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/orders")
@Slf4j
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public ResponseEntity<ResponseData<List<CustomerOrder>>> findAll() {
        return ResponseEntity.ok(ResponseData.createSuccess(orderService.findAll()));
    }

    @PostMapping
    public ResponseEntity<ResponseData<CustomerOrder>> create(@Valid @RequestBody CustomerOrder product) {
        return ResponseEntity.ok(ResponseData.createSuccess(orderService.createOrder(product)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<CustomerOrder>> findById(@PathVariable Long id) {
        CustomerOrder order = orderService.findOrder(id);
        if (order == null) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(ResponseData.createSuccess(order));
    }
}
