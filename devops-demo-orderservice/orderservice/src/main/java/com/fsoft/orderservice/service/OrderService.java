package com.fsoft.orderservice.service;

import com.fsoft.orderservice.entity.CustomerOrder;

import java.util.List;

public interface OrderService {

    CustomerOrder createOrder(CustomerOrder request);

    CustomerOrder findOrder(Long orderId);

    List<CustomerOrder> findAll();

    CustomerOrder duplicateOrder();
}
