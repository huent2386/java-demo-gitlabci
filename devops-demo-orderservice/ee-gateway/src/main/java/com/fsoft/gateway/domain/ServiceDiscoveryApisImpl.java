package com.fsoft.gateway.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.common.models.ResponseData;
import com.fsoft.gateway.constants.GatewayCodeConstants;
import com.fsoft.gateway.models.ServiceManagerDTO;
import com.fsoft.gateway.models.ServiceMapDTO;
import com.fsoft.gateway.utils.YAMLConfig;


@Service
public class ServiceDiscoveryApisImpl implements ServiceDiscoveryApis{
	private static final Logger logger = LoggerFactory.getLogger(ServiceDiscoveryApisImpl.class);
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	YAMLConfig yamlConfig;
	
	@Override
	public ServiceManagerDTO findByClientId(String clientId) {
		HttpHeaders headers = new HttpHeaders();
		
		String url = String.format("%s%s%s", yamlConfig.getServiceDiscoveryUrl(), "/svc-manager/", clientId);
		
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		logger.info(String.format(">>>>>> Url: %s ", url));
		HttpEntity<Void> httpEntity = new HttpEntity<>(headers);
		
		ResponseEntity<ResponseData<ServiceManagerDTO>> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity,
					new ParameterizedTypeReference<ResponseData<ServiceManagerDTO>>() {
			});
		} catch (Exception e) {
			throw new CoreServerRuntimeException(GatewayCodeConstants.UNKNOWN_EXCEPTION, e);
		} 
		
		if(response == null || response.getBody() == null) {
			throw new CoreServerRuntimeException(GatewayCodeConstants.RESPONSE_NULL);
		}
		
		if (!response.getBody().getCode().equals("200")) {
			throw new CoreServerRuntimeException(response.getBody().getCode(), response.getBody().getMessage());
		}
		
		return response.getBody().getData();
	}

	@Override
	public ServiceMapDTO findByClientIdAndSvcCode(String clientId, String svcCode) {
		HttpHeaders headers = new HttpHeaders();
		
		String url = String.format("%s%s%s/%s", yamlConfig.getServiceDiscoveryUrl(), "/svc-map/", clientId, svcCode.toUpperCase());
		
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		logger.info(String.format(">>>>>> Url: %s ", url));
		HttpEntity<Void> httpEntity = new HttpEntity<>(headers);
		
		ResponseEntity<ResponseData<ServiceMapDTO>> response = null;
		try {
			response = restTemplate.exchange(url, HttpMethod.GET, httpEntity,
					new ParameterizedTypeReference<ResponseData<ServiceMapDTO>>() {
			});
		} catch (Exception e) {
			throw new CoreServerRuntimeException(GatewayCodeConstants.UNKNOWN_EXCEPTION, e);
		} 
		
		if(response == null || response.getBody() == null) {
			throw new CoreServerRuntimeException(GatewayCodeConstants.RESPONSE_NULL);
		}
		
		if (!response.getBody().getCode().equals("200")) {
			throw new CoreServerRuntimeException(response.getBody().getCode(), response.getBody().getMessage());
		}
		
		return response.getBody().getData();
	}

}
