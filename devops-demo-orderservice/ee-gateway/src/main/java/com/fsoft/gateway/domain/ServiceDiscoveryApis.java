package com.fsoft.gateway.domain;

import com.fsoft.gateway.models.ServiceManagerDTO;
import com.fsoft.gateway.models.ServiceMapDTO;

public interface ServiceDiscoveryApis {
	public ServiceManagerDTO findByClientId(String clientId);
	public ServiceMapDTO findByClientIdAndSvcCode(String clientId, String svcCode);
}
