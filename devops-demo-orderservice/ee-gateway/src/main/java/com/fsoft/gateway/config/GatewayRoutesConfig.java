package com.fsoft.gateway.config;

import static org.springframework.cloud.gateway.filter.RouteToRequestUrlFilter.ROUTE_TO_URL_FILTER_ORDER;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fsoft.gateway.constants.DataConstants;

@Configuration
public class GatewayRoutesConfig{

    public static final String FORWARDED_URL = "X-CF-Forwarded-Url";

    public static final String PROXY_METADATA = "X-CF-Proxy-Metadata";

    public static final String PROXY_SIGNATURE = "X-CF-Proxy-Signature";
    
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder, 
    		SimpleLoggingFilter loggingFilter, RouteServiceForwardingFilter forwardingFilter) {
		return builder.routes()
				.route("default", r -> r.header(DataConstants.ClientID, ".*")
						.and()
						.header(DataConstants.Token, ".*")
						.filters(f -> {
//								f.stripPrefix(1);
								f.filter(loggingFilter);
								f.filter(forwardingFilter, ROUTE_TO_URL_FILTER_ORDER + 1);
								return f;
						})
						.uri("http://airline-gateway.justfollowme.cloud"))
				.build();
    }
}