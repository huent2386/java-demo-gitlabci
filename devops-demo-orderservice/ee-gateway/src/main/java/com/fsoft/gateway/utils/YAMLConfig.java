package com.fsoft.gateway.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "app")
public class YAMLConfig {

	private String serviceDiscoveryUrl;

}
