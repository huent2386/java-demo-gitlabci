package com.fsoft.gateway.config;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.common.models.ResponseData;
import com.fsoft.common.utils.CommonUtils;
import com.fsoft.gateway.constants.DataConstants;
import com.fsoft.gateway.domain.ServiceDiscoveryApis;
import com.fsoft.gateway.models.ServiceMapDTO;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class RouteServiceForwardingFilter implements GatewayFilter {
	private static final Logger log = LoggerFactory.getLogger(RouteServiceForwardingFilter.class);
    public static final String FORWARDED_URL = "X-CF-Forwarded-Url";

    public static final String PROXY_METADATA = "X-CF-Proxy-Metadata";

    public static final String PROXY_SIGNATURE = "X-CF-Proxy-Signature";

    
    private DataBufferFactory dataBufferFactory = new DefaultDataBufferFactory();
    
    @Autowired
    private ServiceDiscoveryApis serviceDiscoveryApis;
    
    private StringBuilder cached = new StringBuilder();
    
    private byte[] readAndCache(DataBuffer dataBuffer) {
        byte[] data = new byte[dataBuffer.readableByteCount()];
        dataBuffer.asByteBuffer().get(data);
        String chunk = new String(data, StandardCharsets.UTF_8);
        cached.append(chunk);
        
        log.info("===== " + cached.toString());
        return data;
    }
    
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        try {
        	log.info(">>>>>>> START filter: " + exchange.getRequest().getURI());
        	log.info(">>>>>> ServerWebExchange" +  CommonUtils.toJsonString(exchange));
        	log.info(">>>>>> Exchange Request" + CommonUtils.toJsonString(exchange.getRequest()));
        	log.info(">>>>>> Header" + CommonUtils.toJsonString(exchange.getRequest().getHeaders()));
        	log.info(">>>>>> URI" + CommonUtils.toJsonString(exchange.getRequest().getURI()));
            String clientId = exchange.getRequest().getHeaders().get(DataConstants.ClientID).get(0);
            // TODO: Check TOken
            String token = exchange.getRequest().getHeaders().get(DataConstants.Token).get(0);
            String apiRoute = exchange.getRequest().getURI().getPath();
            List<String> routesArr = Arrays.asList(apiRoute.split("/")).stream()
            					.filter(item -> StringUtils.isNotEmpty(item)).collect(Collectors.toList());
            String serviceCode = routesArr.get(0);
            String apiRouting = routesArr.stream().skip(1).collect(Collectors.joining("/"));
            
            String apiQuery = exchange.getRequest().getURI().getQuery();
            apiQuery = apiQuery == null ? "" : "?" + apiQuery;
            apiRouting += apiQuery;
            String username = token;
            
            // TODO: Check status and Expiration of ClientId
            
            ServiceMapDTO svcManager = serviceDiscoveryApis.findByClientIdAndSvcCode(clientId, serviceCode);
            
//            ServerHttpRequest request = exchange.getRequest().mutate().
//                        header("ClientId", "ABAB").
//                        build();
////            exchange.getRequest().getBody().map(item -> item.)
//            exchange = exchange.mutate().request(request).build();
            log.info(">>>> ClientId: " + clientId + " , token: " + token + " , apiRoute: " + apiRoute + " , username: " + username);
            
            
            String urlConnect = svcManager.getSvcUrl() + apiRouting;	
            
            ServerHttpResponse response = exchange.getResponse();
//            DataBufferFactory dataBufferFactory = response.bufferFactory();

            ServerHttpResponseDecorator decoratedResponse = new ServerHttpResponseDecorator(response) {

              @Override
              public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
            	  if (body instanceof Flux) {
                      Flux<? extends DataBuffer> fluxBody = (Flux<? extends DataBuffer>) body;
                      return super.writeWith(fluxBody.doOnNext(dataBuffer -> getDelegate().bufferFactory().wrap(readAndCache(dataBuffer))));
                  }

                  return super.writeWith(body);
              }
            };
            
//            exchange = exchange.mutate().response(decoratedResponse).build();            	
            log.info(">>>>> forwardUrl: " + urlConnect);
            exchange.getAttributes().put(ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR, new URI(urlConnect));
            return chain.filter(exchange.mutate().response(decoratedResponse).build());
        } catch (CoreServerRuntimeException e) {
            ServerHttpResponse response = exchange.getResponse();
    		response.setStatusCode(HttpStatus.OK);
    		response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
    		DataBuffer buf = dataBufferFactory.wrap(CommonUtils.toJsonString(ResponseData.createError(e.getErrCode(), e.getErrMsg())).getBytes(StandardCharsets.UTF_8));
    		return response.writeWith(Flux.just(buf));
        	
        } catch (Exception e) {
        	return Mono.empty();
        }
    }
}