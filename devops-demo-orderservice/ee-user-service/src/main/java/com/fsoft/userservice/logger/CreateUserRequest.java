package com.fsoft.userservice.logger;

import lombok.Data;

@Data
public class CreateUserRequest {
    private String username;
    private String password;
    private String fullName;
    private Integer age;
}
