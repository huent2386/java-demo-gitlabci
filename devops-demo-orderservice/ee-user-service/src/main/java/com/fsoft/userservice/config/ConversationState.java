package com.fsoft.userservice.config;

public class ConversationState {
	private static ThreadLocal<ConversationState> state = new ThreadLocal<>();

	private String accessToken;

	public static String getAccessToken() {
		return state.get() == null ? null : state.get().accessToken;
	}

	public static void setAccessToken(String accessToken) {
		if (state.get() == null) {
			state.set(new ConversationState());
		}

		state.get().accessToken = accessToken;
	}

	public static void remove() {
		state.remove();
	}
}
