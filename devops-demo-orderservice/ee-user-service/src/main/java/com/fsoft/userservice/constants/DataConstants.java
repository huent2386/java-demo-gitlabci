package com.fsoft.userservice.constants;

public interface DataConstants {
	public interface HeaderConst{
		public static final String AUTHORIZATION = "Authorization";
		public static final String BASIC = "Basic";
		public static final String CLIENT_ID = "ClientId";
	}
}
