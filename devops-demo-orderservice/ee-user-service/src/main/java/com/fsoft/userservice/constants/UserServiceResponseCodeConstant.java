package com.fsoft.userservice.constants;

import com.fsoft.common.models.ErrorCodeConstant;

public enum UserServiceResponseCodeConstant implements ErrorCodeConstant {
	EXCEPTION("US-3001", ""),
	DATA_INVALID("US-3002", "Data invalid (US-3002)"),
	CLIENT_ID_INVALID("US-3003", "ClientID invalid (US-3003)"),
	TOKEN_INVALID("US-3004", "Token invalid (US-3004)"),
	PERMISSION_DENIED("US-3005", "Permission denied (US-3005)"),
	USER_NOT_FOUND("US-3006", "User not found (US-3006)"),
	USER_EXPIRED("US-3007", "User is expired (US-3007)"),
	SVC_CODE_NOT_FOUND("US-3008", "Service not found (US-3008)"),
	RESPONSE_NULL("US-3009", "Response null (US-3009)"),
	USER_OR_PASSWORD_INVALID("US-3010", "Username or password invalid (US-3010)"),
	
	UNKNOWN_EXCEPTION("US-4001", "System Error!!! (US-4001)");

	private String value;
	private String display;
	UserServiceResponseCodeConstant(String value, String display) {
		this.value = value;
		this.display = display;
	}
	@Override
	public String getValue() {
		return value;
	}
	@Override
	public String getDisplay() {
		return display;
	}
	
}
