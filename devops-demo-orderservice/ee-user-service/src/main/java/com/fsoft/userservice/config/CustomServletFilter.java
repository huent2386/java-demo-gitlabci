//package com.fsoft.userservice.config;
//
//import java.io.IOException;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.filter.GenericFilterBean;
//
//import com.fsoft.common.models.CoreServerRuntimeException;
//import com.fsoft.redis.RedisManagerService;
//import com.fsoft.userservice.constants.UserServiceResponseCodeConstant;
//
//@Component
//public class CustomServletFilter extends GenericFilterBean {
//
//	@Autowired
//	RedisManagerService redisManagerService;
//
//	@Override
//	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
//			throws IOException, ServletException {
//		try {
//			HttpServletRequest httpRequest = (HttpServletRequest) request;
//			String token = httpRequest.getHeader("Token");
//			if(StringUtils.isEmpty(token)) {
//				throw new CoreServerRuntimeException(UserServiceResponseCodeConstant.TOKEN_INVALID);
//			}
//
//			String username = redisManagerService.getValue(token, String.class);
//			if(StringUtils.isEmpty(username)) {
//				throw new CoreServerRuntimeException(UserServiceResponseCodeConstant.TOKEN_INVALID);
//			}
//
//			ConversationState.setAccessToken(token);
//			chain.doFilter(request, response);
//		}finally {
//			ConversationState.remove();
//		}
//
//	}
//
//}
