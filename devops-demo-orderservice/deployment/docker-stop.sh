#!/usr/bin/env bash

#set -e

docker stop ee-service-discovery;

docker rm ee-authen;
docker rm ee-service-discovery;
docker rm ee-integration-fhu;
docker rm ee-authentication;

docker rmi ee-authen;
docker rmi ee-service-discovery-image;
docker rmi ee-integration-fhu-image;
docker rmi ee-authentication-image;

docker rmi $(docker images -a)