#!/usr/bin/env bash

#set -e

docker build ee-service-discovery -t ee-service-discovery-image;
docker build ee-integration-fhu -t ee-integration-fhu-image;
docker build ee-authentication -t ee-authentication-image;
docker build ee-cms -t ee-cms-service-image;


#docker push iber.therightway.cloud/ibe-service:0.0.5;

docker run -d --name ee-service-discovery -p 8081:9090 ee-service-discovery-image