package com.fsoft.cms.services;

import java.util.List;

import com.fsoft.cms.entity.UFunctionsEntity;
import com.fsoft.cms.model.request.FunctionRequest;

public interface FunctionService {

	/**
	 * 
	 * findAllFunctionByStatusAndFunc
	 * 
	 * get All Function by Status and father id
	 * 
	 * @param fatherId
	 * @param status
	 * @return List<FunctionEntity>
	 */
	List<UFunctionsEntity> findAllFunctionByStatusAndFather(int fatherId, int status);
	
	/**
	 * 
	 * findFunctionById
	 * 
	 * get Function detail by id
	 * 
	 * @param id
	 * @return FunctionEntity
	 */
	UFunctionsEntity findFunctionById(long id);
	
	/**
	 * 
	 * updateFunctionById
	 * 
	 * update Function by id
	 * 
	 * @param UFunctionsEntity request
	 */
	void updateFunctionById(UFunctionsEntity request);

	/**
	 * 
	 * addFunction
	 * 
	 * add FunctionEntity
	 * 
	 * @param UFunctionsEntity request
	 */
	void addFunction(UFunctionsEntity request);

	/**
	 * 
	 * deleteFunction
	 * 
	 * delete FunctionEntity by id
	 * 
	 * @param FunctionRequest request
	 */
	void deleteFunction(FunctionRequest request);
}
