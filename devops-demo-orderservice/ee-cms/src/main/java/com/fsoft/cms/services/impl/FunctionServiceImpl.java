package com.fsoft.cms.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.cms.entity.UFunctionsEntity;
import com.fsoft.cms.enums.ResponseCodeConstant;
import com.fsoft.cms.model.request.FunctionRequest;
import com.fsoft.cms.repository.FunctionRepository;
import com.fsoft.cms.services.FunctionService;
import com.fsoft.common.models.CoreServerRuntimeException;

@Service
public class FunctionServiceImpl implements FunctionService{

	@Autowired
	private FunctionRepository functionRepository;

	@Override
	public List<UFunctionsEntity> findAllFunctionByStatusAndFather(int fatherId, int status) {
		try {
			return functionRepository.findAllByRules(fatherId, status);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.FUNCTION_EXSITS_NOT, e);
		}
	}

	@Override
	public UFunctionsEntity findFunctionById(long id) {
		try {
			return functionRepository.findFunctionById(id);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.FUNCTION_EXSITS_NOT, e);
		}
	}

	@Override
	public void updateFunctionById(UFunctionsEntity request) {

			Optional<UFunctionsEntity> data = functionRepository.findById(request.getId());
			
			if (!data.isPresent()) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.FUNCTION_EXSITS_NOT);
			}
			
			UFunctionsEntity entity = data.get(); 
			
			entity.setFunctionName(request.getFunctionName());
			entity.setFunctionCode(request.getFunctionCode());
			entity.setUrl(request.getUrl());
			entity.setIsDisplay(request.getIsDisplay());
			entity.setIsActive(request.getIsActive());
	//		entity.setCreatedDate(request.getCreatedDate());
			entity.setFatherID(request.getFatherID());
			entity.setPriority(request.getPriority());
			entity.setIcon(request.getIcon());

		try {
			functionRepository.save(entity);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.FUNCTION_EXSITS_NOT, e);
		}
	}

	@Override
	public void addFunction(UFunctionsEntity request) {

		UFunctionsEntity entity = functionRepository.findFunctionByFuncCode(request.getFunctionCode());
		
		if (entity != null) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.FUNCTION_EXSITS);
		}
		
		try {
			functionRepository.save(request);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.FUNCTION_ERROR, e);
		}
	}

	@Override
	public void deleteFunction(FunctionRequest request) {
		try {
			functionRepository.deleteById(request.getFuncId());
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.FUNCTION_EXSITS_NOT, e);
		}
	}

}
