package com.fsoft.cms.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fsoft.cms.entity.CommentsEntity;
import com.fsoft.cms.entity.NewsEntity;
import com.fsoft.cms.enums.ResponseCodeConstant;
import com.fsoft.cms.model.response.CommentsResponse;
import com.fsoft.cms.repository.CommentsRepository;
import com.fsoft.cms.repository.NewsRepository;
import com.fsoft.cms.services.CommentsService;
import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.common.models.ResponseData;

@Service
public class CommentsServiceImpl implements CommentsService {

	private static Logger logger = LoggerFactory.getLogger(ResponseData.class);

	@Autowired
	private CommentsRepository commentsRepository;
	
	@Autowired
	private NewsRepository newsRepository;

	@Override
	public List<CommentsResponse> findCommentByNewsId(long newsId, int page, int pageSize) {
		Sort sortable = Sort.by("createdDate").ascending();
		Pageable pageable = PageRequest.of(page - 1, pageSize, sortable);
		List<CommentsResponse> lst = new ArrayList<>();
		try {
			Page<CommentsEntity> pages = commentsRepository.findCommentByNewsId(newsId, 0, pageable);

			List<CommentsEntity> lstTmp = pages.getContent();
			Pageable pageableSubComment = PageRequest.of(0, 5, sortable);

			for (CommentsEntity commentItem : lstTmp) {
				CommentsResponse tmpItem = new CommentsResponse();
				tmpItem.setComment(commentItem);

				if (commentItem.getIsHaveChild() == 1) {
					Page<CommentsEntity> pageSubComment = commentsRepository.findCommentByNewsId(newsId,
							commentItem.getId(), pageableSubComment);
					tmpItem.setSubComments(pageSubComment.getContent());

					logger.info(String.format(">>> findCommentByNewsId get SubComment INFO: %s", tmpItem.getSubComments()));
				}

				lst.add(tmpItem);
			}

		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.COMMENT_ERROR);
		}

		return lst;
	}

	@Override
	public List<CommentsEntity> findSubCommentByCommentId(long newsId, long commentId, int page, int pageSize) {
		Sort sortable = Sort.by("createdDate").ascending();
		Pageable pageable = PageRequest.of(page - 1, pageSize, sortable);
		try {
			Page<CommentsEntity> pages = commentsRepository.findCommentByNewsId(newsId, commentId, pageable);
			
			return pages.getContent();
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.COMMENT_ERROR);
		}
	}

	@Override
	public void addNewComment(CommentsEntity request) {
		long fatherId = request.getFatherId();
		if(fatherId < 0) {
			fatherId = 0;
		}
		if(fatherId > 0) {
			// Update Trạng thái isHaveChild
			Optional<CommentsEntity> dataCommentFather = commentsRepository.findById(request.getFatherId());
			if(dataCommentFather.isPresent() && dataCommentFather.get() != null 
					&& dataCommentFather.get().getIsHaveChild() != 1) {
				CommentsEntity entityCommentFather = dataCommentFather.get();
				entityCommentFather.setIsHaveChild(1);
				try {
					commentsRepository.save(entityCommentFather);
				} catch (Exception e) {	
				}
			}
		}

		try {
			// Update Count Commnent
			Optional<NewsEntity> dataNews = newsRepository.findById(request.getItemId());
			if (dataNews.isPresent()) {
				NewsEntity entityNews = dataNews.get();
				entityNews.setCommentCount(entityNews.getCommentCount() + 1);
					newsRepository.save(entityNews);
			} 
			commentsRepository.save(request);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.COMMENT_ERROR);
		}
	}
		
	@Override
	public void updateNewComment(CommentsEntity request) {
		try {
			Optional<CommentsEntity> dataComment = commentsRepository.findById(request.getId());
			if(!dataComment.isPresent()) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.COMMENT_EXSITS_NOT);
			}

			CommentsEntity entity = dataComment.get();
			
			entity.setContent(request.getContent());
			
			commentsRepository.save(entity);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.COMMENT_ERROR);
		}
	}

}
