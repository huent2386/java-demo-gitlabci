package com.fsoft.cms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.cms.entity.CategoriesEntity;

@Repository
public interface CategoriesRepository extends JpaRepository<CategoriesEntity, Long> {
	
	@Query("SELECT c FROM CategoriesEntity c "
			+ "				WHERE (:status = -1 OR c.status = :status)"
			+ "				AND (:isHot = -1 OR c.isHot = :isHot)"
			+ "				AND (:type = -1 OR c.type = :type)")
	public List<CategoriesEntity> findByRules(
			@Param("status") int status,
			@Param("isHot") int isHot,
			@Param("type") int type);
}