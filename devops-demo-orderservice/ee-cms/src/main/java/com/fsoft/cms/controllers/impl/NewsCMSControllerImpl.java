package com.fsoft.cms.controllers.impl;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.NewsCMSController;
import com.fsoft.cms.entity.NewsEntity;
import com.fsoft.cms.model.request.GetListNewsRequest;
import com.fsoft.cms.model.response.NewsCMSResponse;
import com.fsoft.cms.model.response.NewsResponse;
import com.fsoft.cms.services.NewsService;
import com.fsoft.common.models.ResponseData;

@RestController
public class NewsCMSControllerImpl implements NewsCMSController {

	@Autowired
	private NewsService newsService;

	@Override
	public ResponseEntity<ResponseData<List<Long>>> insertNews(
			@Valid @RequestBody List<NewsEntity> request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {
		
		List<Long> tmp = newsService.insertNews(request);

		return ResponseEntity.ok(ResponseData.createSuccess(tmp));
	}

	@Override
	public ResponseEntity<ResponseData<Long>> insertNew(
			@Valid @RequestBody NewsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		long newsId = newsService.insertNew(request);

		return ResponseEntity.ok(ResponseData.createSuccess(newsId));
	}

	@Override
	public ResponseEntity<ResponseData<String>> updateNew(
			@Valid @RequestBody NewsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		newsService.updateNew(request);

		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

	@Override
	public ResponseEntity<ResponseData<String>> deleteNew(
			@Valid @RequestBody NewsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {
		
		newsService.deleteNew(request);

		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}
	
	@Override
	public ResponseEntity<ResponseData<NewsCMSResponse>> findByRule(
			@Valid @RequestBody GetListNewsRequest request) {
		
		NewsCMSResponse userModel = newsService.findByRuleForCMS(request);

		return ResponseEntity.ok(ResponseData.createSuccess(userModel));
	}

	@Override
	public ResponseEntity<ResponseData<NewsResponse>> findNewById(
			@Valid @RequestParam(value = "newsId", required = true) Long newsId) {

		NewsResponse entity = newsService.findByNewId(newsId);

		return ResponseEntity.ok(ResponseData.createSuccess(entity));
	}
}
