package com.fsoft.cms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.cms.entity.LikesEntity;

public interface LikesRepository extends JpaRepository<LikesEntity, Long>{

}
