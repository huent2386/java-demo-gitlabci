package com.fsoft.cms.controllers;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fsoft.cms.model.request.ActionRequest;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/action")
public interface LikesController {

 	@PostMapping("/act-post")
 	public ResponseEntity<ResponseData<String>> actionPost(
 			@Valid @RequestBody ActionRequest request);

 	@PostMapping("/act-lst")
 	public ResponseEntity<ResponseData<String>> getListLike(
 			@Valid @RequestBody ActionRequest request);
}
