package com.fsoft.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Category")
@DynamicUpdate // Update các trường được chỉ định trong table
public class CategoriesEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private int id;

	@Column(name = "Name", nullable = false)
	private String name;

	@Column(name = "Icon", nullable = false)
	private String icon;

	@Column(name = "Status", nullable = false)
	private int status;

	@Column(name = "IsHot", nullable = false)
	private int isHot;

	@Column(name = "Priority", nullable = false)
	private int priority;

	@Column(name = "Type", nullable = false)
	private int type;

	@Column(name = "FatherId", nullable = false)
	private int fatherId;

	@Column(name = "Path", nullable = false)
	private String path;
}
