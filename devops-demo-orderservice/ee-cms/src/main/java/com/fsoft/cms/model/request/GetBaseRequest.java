package com.fsoft.cms.model.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Data;

@Data
public class GetBaseRequest {
	
	@Min(value = 1, message = "Min page=1")
	@Max(value = 50, message = "Max page=50")
	private int page = 1;
	
	@Min(value = 1, message = "Min pageSize = 1")
	@Max(value = 50, message = "Max pageSize = 50")
	private int pageSize = 20;
}
