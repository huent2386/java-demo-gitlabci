package com.fsoft.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "UserFunctions")
@DynamicUpdate // Update các trường được chỉ định trong table
public class UserFunctionsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long id;
	
	@Column(name = "UserId", nullable = false)
	private long userId;

	@Column(name = "FunctionId", nullable = false)
	private int functionId;

	@Column(name = "IsInsert", nullable = false)
	private int isInsert;
	
	@Column(name = "IsUpdate", nullable = false)
	private int isUpdate;
	
	@Column(name = "IsDelete", nullable = false)
	private int isDelete;
	
	@Column(name = "IsFullControl", nullable = false)
	private int isFullControl;
	
	@Column(name = "FunctionCode", nullable = false)
	private String functionCode;
	
	@Column(name = "UserName", nullable = false)
	private String userName;
}
