package com.fsoft.cms.utils;

public class Const {
	
	// Define Type Item
	public static int TYPE_ITEM_POST = 0;
	public static int TYPE_ITEM_COMMENT = 1;
	public static int TYPE_ITEM_COMMENT_SUB = 2;
	
	// Define Type Like - Unlike
	public static int TYPE_LIKE_UN = 0;
	public static int TYPE_LIKE = 1;
}
