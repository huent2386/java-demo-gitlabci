package com.fsoft.cms.enums;

import com.fsoft.common.models.ErrorCodeConstant;

public enum ResponseCodeConstant implements ErrorCodeConstant {
	
	//////// START MODULE CMS ////////
	EXCEPTION("CMS-5001", "EXCEPTION (CMS-5001)"),
	DATA_INVALID("CMS-5002", "Data invalid (CMS-5002)"),
	CLIENT_ID_INVALID("CMS-5003", "ClientID invalid (CMS-5003)"),
	TOKEN_INVALID("CMS-5004", "Token invalid (CMS-5004)"),
	PERMISSION_DENIED("CMS-5005", "Permission denied (CMS-5005)"),
	USER_NOT_FOUND("CMS-5006", "User not found (CMS-5006)"),
	USER_EXPIRED("CMS-5007", "User is expired (CMS-5007)"),
	SVC_CODE_NOT_FOUND("CMS-5008", "Service not found (CMS-5008)"),
	RESPONSE_NULL("CMS-5009", "Response null (CMS-5009)"),

	USER_ERROR("CMS-5010", "User error (CMS-5010)"),
	USER_EXSITS("CMS-5011", "User is exsits (CMS-5011)"),
	USER_EXSITS_NOT("CMS-5012", "User is not exsits (CMS-5012)"),
	USER_EXSITS_EMAIL("CMS-5013", "Email exsits (CMS-5013)"),

	USER_ACTIVE_NOT("CMS-5014", "User is lock (CMS-5014)"),
	USER_PASSWORD_NOT_MATCH("CMS-5015", "Info user not match (CMS-5015)"),

	FUNCTION_ERROR("CMS-5016", "Function error (CMS-5016)"),
	FUNCTION_EXSITS("CMS-5017", "Function is exsits (CMS-5017)"),
	FUNCTION_EXSITS_NOT("CMS-5018", "Function is not exsits (CMS-5018)"),

	USER_FUNCTION_ERROR("CMS-5019", "User Function error (CMS-5019)"),
	USER_FUNCTION_EXSITS("CMS-5020", "User Function is exsits (CMS-5020)"),
	USER_FUNCTION_EXSITS_NOT("CMS-5021", "User Function is not exsits (CMS-5021)"),
	
	NEWS_ERROR("CMS-5022", "News error (CMS-5022)"),
	NEWS_EXSITS("CMS-5023", "News is exsits (CMS-5023)"),
	NEWS_EXSITS_NOT("CMS-5024", "News is not exsits (CMS-5024)"),

	CATRGORIES_ERROR("CMS-5025", "Categories error (CMS-5025)"),
	CATRGORIES_EXSITS("CMS-5026", "Categories is exsits (CMS-5026)"),
	CATRGORIES_EXSITS_NOT("CMS-5027", "Categories is not exsits (CMS-5027)"),

	COMMENT_ERROR("CMS-5028", "Comment error (CMS-5028)"),
	COMMENT_EXSITS("CMS-5029", "Comment is exsits (CMS-5029)"),
	COMMENT_EXSITS_NOT("CMS-5030", "Comment is not exsits (CMS-5030)"),
	
	LIKE_ERROR("CMS-5031", "Like error (CMS-5031)"),
	
	
	UNKNOWN_EXCEPTION("CMS-5000", "System Error!!! (CMS-5000)");
	////////END MODULE CMS ////////

	private String value;
	private String display;
	ResponseCodeConstant(String value, String display) {
		this.value = value;
		this.display = display;
	}
	@Override
	public String getValue() {
		return value;
	}
	@Override
	public String getDisplay() {
		return display;
	}
	
}
