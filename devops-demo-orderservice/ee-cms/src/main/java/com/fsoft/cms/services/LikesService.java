package com.fsoft.cms.services;

import com.fsoft.cms.model.request.ActionRequest;

public interface LikesService {

	/**
	 * 
	 * actionPost
	 * 
	 * an action for post or comment
	 * 
	 * @param ActionRequest request
	 */
	void actionPost(ActionRequest request);
}
