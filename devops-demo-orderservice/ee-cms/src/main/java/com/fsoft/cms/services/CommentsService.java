package com.fsoft.cms.services;


import java.util.List;

import com.fsoft.cms.entity.CommentsEntity;
import com.fsoft.cms.model.response.CommentsResponse;

public interface CommentsService {

	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// SERVER START ///////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// SERVER END /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	
	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// CLIENT START ///////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 * addNewComment
	 * 
	 * Add new comment
	 * 
	 * @param CommentsEntity request
	 * 
	 */
	void addNewComment(CommentsEntity request);

	/**
	 * 
	 * updateNewComment
	 * 
	 * update new comment
	 * 
	 * @param CommentsEntity request
	 * 
	 */
	void updateNewComment(CommentsEntity request);
	
	/**
	 * 
	 * findCommentByNewsId
	 * 
	 * Get All CommentsResponse by newsId, commentId
	 * 
	 * @param newsId
	 * 
	 * @return List<CommentsResponse>
	 */
	List<CommentsResponse> findCommentByNewsId(long newsId, int page, int pageSize);

	/**
	 * 
	 * findSubCommentByCommentId
	 * 
	 * Get All CommentsResponse by newsId, commentId
	 * 
	 * @param newsId
	 * @param commentId
	 * 
	 * @return List<CommentsEntity>
	 */
	List<CommentsEntity> findSubCommentByCommentId(long newsId, long commentId, int page, int pageSize);
	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// CLIENT END /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}
