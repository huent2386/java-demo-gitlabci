package com.fsoft.cms.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fsoft.cms.entity.CategoriesEntity;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/cat")
public interface CategoriesClientController {
	
	@GetMapping("/getListCat")
	public ResponseEntity<ResponseData<List<CategoriesEntity>>> findByStatusAndType(
			@Valid @RequestParam(value = "status", required = true) Integer status,
			@Valid @RequestParam(value = "type", required = true) Integer type);
}
