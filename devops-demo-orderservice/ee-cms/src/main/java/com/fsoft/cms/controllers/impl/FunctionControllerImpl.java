package com.fsoft.cms.controllers.impl;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.FunctionController;
import com.fsoft.cms.entity.UFunctionsEntity;
import com.fsoft.cms.model.request.FunctionRequest;
import com.fsoft.cms.services.FunctionService;
import com.fsoft.common.models.ResponseData;

@RestController
public class FunctionControllerImpl implements FunctionController {

	@Autowired
	private FunctionService functionService;

	@Override
	public ResponseEntity<ResponseData<List<UFunctionsEntity>>> findAllFunctionByStatusAndFather(
			@Valid @RequestParam(value = "fatherId", required = true) Integer fatherId,
			@Valid @RequestParam(value = "status", required = true) Integer status) {
		
		List<UFunctionsEntity> lst = functionService.findAllFunctionByStatusAndFather(fatherId, status);
		
		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}

	@Override
	public ResponseEntity<ResponseData<UFunctionsEntity>> findFunctionById(
			@Valid @RequestParam(value = "funcId", required = true) Long funcId) {

		UFunctionsEntity entity = functionService.findFunctionById(funcId);
		
		return ResponseEntity.ok(ResponseData.createSuccess(entity));
	}

	@Override
	public ResponseEntity<ResponseData<String>> updateFunctionById(
			@Valid @RequestBody UFunctionsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		functionService.updateFunctionById(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

	@Override
	public ResponseEntity<ResponseData<String>> addFunction(
			@Valid @RequestBody UFunctionsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		functionService.addFunction(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

	@Override
	public ResponseEntity<ResponseData<String>> deleteFunction(
			@Valid @RequestBody FunctionRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		functionService.deleteFunction(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}
}
