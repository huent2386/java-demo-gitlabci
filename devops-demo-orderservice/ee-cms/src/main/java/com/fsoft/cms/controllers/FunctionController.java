package com.fsoft.cms.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fsoft.cms.entity.UFunctionsEntity;
import com.fsoft.cms.entity.UserEntity;
import com.fsoft.cms.model.request.FunctionRequest;
import com.fsoft.cms.model.request.UserRequest;
import com.fsoft.cms.model.response.UserResponse;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/func")
public interface FunctionController {

	@GetMapping("/getListFunc")
	public ResponseEntity<ResponseData<List<UFunctionsEntity>>> findAllFunctionByStatusAndFather(
			@Valid @RequestParam(value = "fatherId", required = true) Integer fatherId,
			@Valid @RequestParam(value = "status", required = true) Integer status);

	@GetMapping("/getDetailFunc")
	public ResponseEntity<ResponseData<UFunctionsEntity>> findFunctionById(
			@Valid @RequestParam(value = "funcId", required = true) Long funcId);

	@PostMapping("/update")
	public ResponseEntity<ResponseData<String>> updateFunctionById(
			@Valid @RequestBody UFunctionsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@PostMapping("/add")
	public ResponseEntity<ResponseData<String>> addFunction(
			@Valid @RequestBody UFunctionsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@PostMapping("/delete")
	public ResponseEntity<ResponseData<String>> deleteFunction(
			@Valid @RequestBody FunctionRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);
}
