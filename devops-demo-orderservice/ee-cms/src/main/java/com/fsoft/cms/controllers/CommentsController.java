package com.fsoft.cms.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fsoft.cms.entity.CommentsEntity;
import com.fsoft.cms.model.response.CommentsResponse;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/comments")
public interface CommentsController {

 	@PostMapping("/addComment")
 	public ResponseEntity<ResponseData<String>> addNewComment(
 			@Valid @RequestBody CommentsEntity request);

 	@PostMapping("/updateComment")
 	public ResponseEntity<ResponseData<String>> updateNewComment(
 			@Valid @RequestBody CommentsEntity request);
	
 	@GetMapping("/getListComment")
 	public ResponseEntity<ResponseData<List<CommentsResponse>>> findCommentByNewsId(
 			@Valid @RequestParam(value = "newsId", required = true) Long newsId,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize);

 	@GetMapping("/getListSubComment")
 	public ResponseEntity<ResponseData<List<CommentsEntity>>> findSubCommentByCommentId(
 			@Valid @RequestParam(value = "newsId", required = true) Long newsId,
 			@Valid @RequestParam(value = "commentId", required = true) Long commentId,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize);
}
