package com.fsoft.cms.model.response;


import java.util.List;

import com.fsoft.cms.entity.NewsEntity;

import lombok.Data;

@Data
public class NewsClientResponse {

	private int catId;
	private String catName;
	private List<NewsEntity> news;
}
