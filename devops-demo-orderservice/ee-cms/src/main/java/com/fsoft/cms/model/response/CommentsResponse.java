package com.fsoft.cms.model.response;


import java.util.ArrayList;
import java.util.List;

import com.fsoft.cms.entity.CommentsEntity;

import lombok.Data;

@Data
public class CommentsResponse {
	private CommentsEntity comment;
	private List<CommentsEntity> subComments = new ArrayList<>();
}
