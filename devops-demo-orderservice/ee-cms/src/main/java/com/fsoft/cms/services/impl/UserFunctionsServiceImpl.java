package com.fsoft.cms.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fsoft.cms.entity.UserFunctionsEntity;
import com.fsoft.cms.enums.ResponseCodeConstant;
import com.fsoft.cms.model.request.UserFunctionsRequest;
import com.fsoft.cms.repository.UserFunctionsRepository;
import com.fsoft.cms.services.UserFunctionsService;
import com.fsoft.common.models.CoreServerRuntimeException;

@Service
public class UserFunctionsServiceImpl implements UserFunctionsService {

	@Autowired
	private UserFunctionsRepository userFunctionsRepository;

	@Override
	public List<UserFunctionsEntity> findAllUserFunctionsByUIdAndUName(long uId, String uName) {
		try {
			return userFunctionsRepository.findAllUserFunctionsByUIdAndUName(uId, uName);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_FUNCTION_EXSITS_NOT, e);
		}
	}

	@Transactional
	@Override
	public void updateUserFunctionsByUIdAndUName(UserFunctionsRequest request) {
		try {

			userFunctionsRepository.deleteAllPermissionByUIdAndUName(request.getUserId(), request.getUserName());

			userFunctionsRepository.saveAll(request.getLstUserPermission());

		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_FUNCTION_ERROR, e);
		}
	}

}
