package com.fsoft.cms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.cms.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

	@Query("SELECT u FROM UserEntity u WHERE u.email = :email")
	public UserEntity findUserByEmail( 
			@Param("email") String email);
	
	@Query("UPDATE UserEntity u SET u.status = (CASE WHEN u.status = 0 THEN 1 ELSE 0 END) WHERE u.id = :id AND u.userName = :userName")
	public void updateUserStatus( 
			@Param("id") long id,
			@Param("userName") String userName);

	@Query("UPDATE UserEntity u SET u.password = :password WHERE u.id = :id AND u.userName = :userName")
	public void changePasswordUser( 
			@Param("id") long id,
			@Param("userName") String userName,
			@Param("password") String password);

	@Query("SELECT u FROM UserEntity u WHERE u.userName = :userName")
	public UserEntity findUserByUserName( 
			@Param("userName") String userName);

	@Query("SELECT new UserEntity(" 
						+ "u.id, " 
						+ "u.userName, "  
						+ "u.fullName, "  
						+ "u.email, "  
						+ "u.type, "  
						+ "u.status, "  
						+ "u.createdUser, "  
						+ "u.createdDate) FROM UserEntity u WHERE u.fullName LIKE %:fullName% AND u.status = :status")
	public Page<UserEntity> findAllUserByKeyword(
			@Param("fullName") String fullName,
			@Param("status") int status,
			Pageable pageable);

	@Query("SELECT new UserEntity(" 
						+ "u.id, " 
						+ "u.userName, "  
						+ "u.fullName, "  
						+ "u.email, "  
						+ "u.type, "  
						+ "u.status, "  
						+ "u.createdUser, "  
						+ "u.createdDate) FROM UserEntity u WHERE u.status = :status")
	public Page<UserEntity> findAllUserByKeyword(
			@Param("status") int status,
			Pageable pageable);
	
	@Query("SELECT new UserEntity(" 
						+ "u.id, " 
						+ "u.userName, "  
						+ "u.fullName, "  
						+ "u.email, "  
						+ "u.type, "  
						+ "u.status, "  
						+ "u.createdUser, "  
						+ "u.createdDate) FROM UserEntity u WHERE u.fullName LIKE %:fullName%")
	public Page<UserEntity> findAllUserByKeyword(
			@Param("fullName") String fullName,
			Pageable pageable);

	@Query("SELECT new UserEntity(" 
						+ "u.id, " 
						+ "u.userName, "  
						+ "u.fullName, "  
						+ "u.email, "  
						+ "u.type, "  
						+ "u.status, "  
						+ "u.createdUser, "  
						+ "u.createdDate) FROM UserEntity u")
	public Page<UserEntity> findAllUserByKeyword(
			Pageable pageable);
}