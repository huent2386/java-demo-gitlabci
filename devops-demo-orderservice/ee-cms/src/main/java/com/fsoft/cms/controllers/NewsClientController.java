package com.fsoft.cms.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fsoft.cms.entity.NewsEntity;
import com.fsoft.cms.model.response.NewsClientResponse;
import com.fsoft.cms.model.response.NewsResponse;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/news")
public interface NewsClientController {
	
	@GetMapping("/getListNews")
	public ResponseEntity<ResponseData<List<NewsResponse>>> findByRule(
			@Valid @RequestParam(value = "categoryId", required = true) Integer categoryId,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize);
	
	@GetMapping("/getDetail")
	public ResponseEntity<ResponseData<NewsResponse>> findNewById(
			@Valid @RequestParam(value = "newsId", required = true) Long newsId);
	
	@GetMapping("/getNewsHome")
	public ResponseEntity<ResponseData<List<NewsClientResponse>>> findNewsHome();
	
	@GetMapping("/getNewsHot")
	public ResponseEntity<ResponseData<List<NewsResponse>>> findNewsHot();

}
