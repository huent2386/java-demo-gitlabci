package com.fsoft.cms.model.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Data;

@Data
public class ActionRequest {
	private String userName;
	private String ip;
	private long itemId;			// Id Of Item
	
	@Max(value = 1, message = "Action like = 1 | unLike = 0")
	@Min(value = 0, message = "Action like = 1 | unLike = 0")
	private int status;				// Status Action (Like|UnLike)
	
	@Max(value = 2, message = "Post type (Post = 0 | comment = 1 | sub comment = 2)")
	@Min(value = 0, message = "Post type (Post = 0 | comment = 1 | sub comment = 2)")
	private int itemType;			// Type Action for Item (Post|Comment|SubComment)
}
