package com.fsoft.cms.controllers.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.LikesController;
import com.fsoft.cms.model.request.ActionRequest;
import com.fsoft.cms.services.LikesService;
import com.fsoft.common.models.ResponseData;

@RestController
public class LikesControllerImpl implements LikesController {

	@Autowired
	private LikesService likesService;
	
	@Override
	public ResponseEntity<ResponseData<String>> actionPost(
			@Valid @RequestBody ActionRequest request) {
		likesService.actionPost(request);
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

	@Override
	public ResponseEntity<ResponseData<String>> getListLike(@Valid ActionRequest request) {
		// TODO Auto-generated method stub
		return null;
	}
}
