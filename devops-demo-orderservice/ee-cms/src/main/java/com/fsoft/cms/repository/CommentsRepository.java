package com.fsoft.cms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.cms.entity.CategoriesEntity;
import com.fsoft.cms.entity.CommentsEntity;

@Repository
public interface CommentsRepository extends JpaRepository<CommentsEntity, Long> {

	@Query("SELECT c FROM CommentsEntity c WHERE c.itemId = :itemId AND c.fatherId = :fatherId AND c.status = 1")
	public Page<CommentsEntity> findCommentByNewsId(
			@Param("itemId") long itemId,
			@Param("fatherId") long fatherId,
			Pageable pageable);
}