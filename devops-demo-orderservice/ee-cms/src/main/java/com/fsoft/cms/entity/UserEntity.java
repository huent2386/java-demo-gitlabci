package com.fsoft.cms.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

@Data
@Entity
@Table(name = "User")
@DynamicUpdate // Update các trường được chỉ định trong table
public class UserEntity {

	public UserEntity(long id, String userName, String fullName, String email,
			int type, int status, String createdUser, LocalDateTime createdDate){
		this.id = id;
		this.userName = userName;
		this.fullName = fullName;
		this.email = email;
		this.type = type;
		this.status = status;
		this.createdUser = createdUser;
		this.createdDate = createdDate;
	}
	
	public UserEntity(long id, String userName, String password, String fullName, String email, int type, int status,
			String createdUser, LocalDateTime createdDate) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.fullName = fullName;
		this.email = email;
		this.type = type;
		this.status = status;
		this.createdUser = createdUser;
		this.createdDate = createdDate;
	}
	
	public UserEntity() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long id;

	@Column(name = "UserName", nullable = false)
	private String userName;

	@Column(name = "Password", nullable = false)
	private String password;

	@Column(name = "FullName", nullable = false)
	private String fullName;

	@Column(name = "Email", nullable = false)
	private String email;

	@Column(name = "Type", nullable = false)
	private int type;

	@Column(name = "Status", nullable = false)
	private int status;

	@Column(name = "CreatedUser", nullable = false)
	private String createdUser;

	@Column(name = "CreatedDate", nullable = false)
	private LocalDateTime createdDate = LocalDateTime.now();
}
