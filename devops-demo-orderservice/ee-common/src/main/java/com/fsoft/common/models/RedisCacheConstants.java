package com.fsoft.common.models;

public interface RedisCacheConstants {
	public static final String GROUP_CACHE_USER = "GROUP_USER";

	public static final String USER_INFO = "USER_INFO";
	public static final String TOKEN_JWT = "TOKEN_JWT";
	public static final String TOKEN_HASH = "TOKEN_HASH";

	public static final String AUTHEN_MANAGER = "AUTHEN_MANAGER";
	public static final String GAME_ENTITIES = "GAME_ENTITIES";
}
