package com.fsoft.common.models;

public interface ErrorCodeConstant {
	String getValue();
	String getDisplay();
}
