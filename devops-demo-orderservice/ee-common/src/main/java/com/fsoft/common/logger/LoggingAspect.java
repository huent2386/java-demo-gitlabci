package com.fsoft.common.logger;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

@Aspect
@Configuration
public class LoggingAspect extends LoggingPointcut {
	private static Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Autowired
	HttpServletRequest request;

	@Around("restcontroller()")
	public Object userAdviceController(ProceedingJoinPoint joinPoint) throws Throwable {
		try {
			logger.info(
					"\n===========================================================================================\n"
							+ "\n===========================================================================================\n");
			logger.info("[CONTROLLER-START] " + joinPoint.getSignature().getDeclaringTypeName() +
					"." + joinPoint.getSignature().getName());
			MethodSignature method = (MethodSignature) joinPoint.getStaticPart().getSignature();
			List<String> params = Arrays.asList(method.getParameterNames());
			StringBuilder requestUrl = new StringBuilder(String.format(" RequestURL --> [%s] ", request.getMethod())).append(request.getRequestURL());
			List<Object> args = Arrays.asList(joinPoint.getArgs());
			if (!StringUtils.isEmpty(request.getQueryString())) {
				requestUrl.append("?").append(request.getQueryString());
			}
			logger.info(requestUrl.toString());

			StringBuilder str = new StringBuilder(" Parameters -->  ");
			Iterator<Object> i1 = args.iterator();
			params.forEach(param -> str.append(", ").append(param).append(" = ").append(i1.next()));
			logger.info(str.toString());

			if (request.getHeaderNames() != null) {
				StringBuilder sb = new StringBuilder(" RequestHeader --> ");
				Enumeration<String> headerNames = request.getHeaderNames();
				while (headerNames.hasMoreElements()) {
					String key = headerNames.nextElement();
					String value = request.getHeader(key);
					sb.append(" ,").append(key).append(":").append(value);
				}
				String ip = request.getHeader("X-FORWARDED-FOR");
				if (ip == null) {
					ip = request.getRemoteAddr();
				}

				sb.append(" ,").append("IP-Client").append(":").append(ip);
				logger.info(sb.toString());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		long t1 = System.currentTimeMillis();

		Object value = joinPoint.proceed();

		try {
			logger.info(String.format("[CONTROLLER-END]: %s.%s : %d", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(), System.currentTimeMillis() - t1));
		} catch (Exception ex) {
		    logger.error(ex.getMessage());
		}

		return value;
	}
}
