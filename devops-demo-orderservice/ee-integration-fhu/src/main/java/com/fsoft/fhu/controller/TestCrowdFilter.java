//package com.fsoft.fhu.controller;
//
//import java.io.IOException;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.stereotype.Service;
//
//import com.atlassian.crowd.integration.http.CrowdHttpAuthenticatorImpl;
//import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelper;
//import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelperImpl;
//import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractor;
//import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractorImpl;
//import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
//import com.atlassian.crowd.service.client.ClientProperties;
//import com.atlassian.crowd.service.client.ClientPropertiesImpl;
//import com.atlassian.crowd.service.client.ClientResourceLocator;
//import com.atlassian.crowd.service.client.CrowdClient;
//import com.fsoft.common.utils.CommonUtils;
//
///**
// *
// * @author turri
// */
//@Service
//public class TestCrowdFilter implements Filter {
//
//	private CrowdClient crowdClient;
//	private CrowdHttpAuthenticatorImpl crowdHttpAuthenticator;
//
//	public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
//		System.out.println("CROWD Client " + crowdClient);
//
//		HttpServletRequest request = (HttpServletRequest) sr;
//		HttpServletResponse response = (HttpServletResponse) sr1;
//		boolean authenticated = false;
//		try {
//			authenticated = crowdHttpAuthenticator. isAuthenticated(request, response);
//			System.out.println("**** User: " + crowdHttpAuthenticator.getUser(request));
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		System.out.println("**** Token: " + crowdHttpAuthenticator.getToken(request));
//
//		System.out.println("Is user authenticated: " + authenticated);
//
//		fc.doFilter(sr, sr1);
//	}
//
//	public void destroy() {
//	}
//
//	public void init(FilterConfig fc) throws ServletException {
//		ClientResourceLocator resourceLocator = new ClientResourceLocator("crowd.properties");
//		ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromResourceLocator(resourceLocator);
//		RestCrowdClientFactory crowdClientFactory = new RestCrowdClientFactory();
//		crowdClient = crowdClientFactory.newInstance(clientProperties);
//		CrowdHttpValidationFactorExtractor validationFactorExtractor = CrowdHttpValidationFactorExtractorImpl
//				.getInstance();
//		CrowdHttpTokenHelper tokenHelper = CrowdHttpTokenHelperImpl.getInstance(validationFactorExtractor);
//		crowdHttpAuthenticator = new CrowdHttpAuthenticatorImpl(crowdClient, clientProperties, tokenHelper);
//
//		System.out.println("Resource Locator " + resourceLocator);
//		System.out.println("Client Properties " + clientProperties);
//	}
//
//}