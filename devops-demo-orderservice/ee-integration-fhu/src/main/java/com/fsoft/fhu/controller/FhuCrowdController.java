package com.fsoft.fhu.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.integration.http.CrowdHttpAuthenticator;
import com.atlassian.crowd.integration.http.CrowdHttpAuthenticatorImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelperImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractorImpl;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;
import com.fsoft.common.models.ResponseData;
import com.fsoft.common.utils.CommonUtils;
import com.fsoft.fhu.DemoSoap;
import com.fsoft.fhu.models.MfsAuthenDTO;
import com.fsoft.fhu.models.MfsUserProfileDTO;
import com.fsoft.fhu.models.UserInfoDTO;
import com.fsoft.fhu.models.UserInfoRequest;
import com.fsoft.fhu.services.FhuCrowdService;
import com.fsoft.fhu.services.FhuCrowdServiceImpl.UserFhu;

@RestController
@RequestMapping("/fhu")
public class FhuCrowdController {

	@Autowired
	HttpServletRequest httpRequest;
	
	@Autowired
	ModelMapper modelMapper;
	
	@GetMapping("/home")
	public ResponseEntity<ResponseData<String>> home(HttpServletResponse response){
		try {
			final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(DemoSoap.getProps());
			final CrowdClient crowdClient = new RestCrowdClientFactory().newInstance(clientProperties);
//					"https://crowd-dev.fsoft.com.vn/crowd", "myfsoft2", "myfsoft2"); //.newInstance(clientProperties);
			final CrowdHttpAuthenticator crowdHttpAuthenticator = new CrowdHttpAuthenticatorImpl(
					crowdClient, clientProperties, 
					CrowdHttpTokenHelperImpl.getInstance(CrowdHttpValidationFactorExtractorImpl.getInstance()));
			
			System.out.println(crowdClient.getCurrentEventToken());
			//			AuthenticationState state = crowdHttpAuthenticator.checkAuthenticated(httpRequest, response);
			com.atlassian.crowd.embedded.api.User usser = crowdClient.authenticateUser("phongnv5", "Hoilamgi@92");
			System.out.println(CommonUtils.toJsonString(usser));
			System.out.println(crowdHttpAuthenticator.checkAuthenticated(httpRequest, response));
			System.out.println(crowdHttpAuthenticator.getToken(httpRequest)); 
			System.out.println(crowdHttpAuthenticator.getUser(httpRequest));
			String temp = crowdClient.authenticateSSOUser(
					new UserAuthenticationContext("phongnv5", 
					new PasswordCredential("Hoilamgi@90"), 
					Arrays.asList(new ValidationFactor("remote_address", "127.0.0.1")).stream().toArray(size -> new ValidationFactor[size]),
					"myfsoft2"));
			
			System.out.println(temp);
			User user = crowdHttpAuthenticator.authenticate(httpRequest, response, "phongnv5", ",Hoilamgi@92");
			System.out.println(CommonUtils.toJsonString(user));
			System.out.println(crowdHttpAuthenticator.getToken(httpRequest));
			
//			System.out.println(user.toString());
		} catch (Exception ex) {
			System.out.println(ex);
		}
		
		return ResponseEntity.ok(ResponseData.createSuccess("This is service authen FHU"));
	}
	
	@Autowired
	@Qualifier("fhuCrowdServiceMock")
	FhuCrowdService fhuCrowdService;
	
	@PostMapping("/user-info")
	public ResponseEntity<ResponseData<MfsAuthenDTO>> index(@Valid @RequestBody UserInfoRequest request){
		MfsAuthenDTO user = fhuCrowdService.userAuthentication(request.getUsername(), request.getPassword());
//		UserInfoDTO response =  modelMapper.map(user, UserInfoDTO.class);
		
		return ResponseEntity.ok(ResponseData.createSuccess(user));
	}
	
	@PostMapping("/user-authen")
	public ResponseEntity<ResponseData<MfsAuthenDTO>> userAuthen(@Valid @RequestBody UserInfoRequest request){
		MfsAuthenDTO user = fhuCrowdService.userAuthentication(request.getUsername(), request.getPassword());
		
		return ResponseEntity.ok(ResponseData.createSuccess(user));
	}
	
	@GetMapping("/user-profiles/{username}")
	public ResponseEntity<ResponseData<MfsUserProfileDTO>> userProfile(
			@PathVariable(name = "username") String username){
		
		MfsUserProfileDTO result = fhuCrowdService.userProfile(username, "");
		return ResponseEntity.ok(ResponseData.createSuccess(result));
	}
	
	@GetMapping("/ping")
	public ResponseEntity<ResponseData<String>> ping(HttpServletResponse response){
		
		return ResponseEntity.ok(ResponseData.createSuccess("PONG! PONG! PONG!"));
	
	}

	
}
