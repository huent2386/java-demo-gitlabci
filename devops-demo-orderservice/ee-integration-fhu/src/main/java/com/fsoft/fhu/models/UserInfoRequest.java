package com.fsoft.fhu.models;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoRequest {
	
	@NotNull(message = "Username is required")
	private String username;
	
	@NotNull(message = "Password is required")
	private String password;
	
	
	@JsonIgnoreProperties
	public String getPassword() {
		return password;
	}


	@Override
	public String toString() {
		return "UserInfoRequest [username=" + username + "]";
	}
	
	
}
