package com.fsoft.fhu.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fsoft.fhu.models.MfsAuthenDTO;
import com.fsoft.fhu.models.MfsUserProfileDTO;

@Service
@Qualifier("fhuCrowdServiceMock")
public class FhuCrowdServiceImplMock implements FhuCrowdService {
	private static Logger logger = LoggerFactory.getLogger(FhuCrowdServiceImplMock.class);

	@Override
	public void applicationAuthenticate(String application, String password) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MfsAuthenDTO userAuthentication(String username, String password) {
		logger.info("Username: " + username + " - password: " +  password);
		String fhuToken = "123456789";
		String token = UUID.randomUUID().toString();
		
		MfsAuthenDTO authenDTO = MfsAuthenDTO.builder()
											.accessToken(token)
											.fhuToken(fhuToken)
											.build();
		
//		UserInfoDTO userInfo = UserInfoDTO.builder().firstName("Phong").lastName("Nguyen").email("phongnv5@fsoft.com.vn").displayName("PhongNV5").build();
//		Map<Object, Object> mapCache = new HashMap<>();
//		mapCache.put("TOKEN", token);
//		mapCache.put("FHU_TOKEN", fhuToken);
//		mapCache.put("USER_INFO", userInfo);
//		redisManagerService.setValue(username, mapCache, 86400);
		
//		UserInfoDTO cacheUser = redisManagerService.getValue(username, "USER_INFO", UserInfoDTO.class);
//		logger.info("CacheUser: " + cacheUser);
//		
//		String cacheToken = redisManagerService.getValue(username, "TOKEN", String.class);
//		logger.info("CacheToken: " + cacheToken);
//		
//		String cacheFhuToken = redisManagerService.getValue(username, "FHU_TOKEN", String.class);
//		logger.info("CacheFHUToken:" + cacheFhuToken);
		
		
		return authenDTO;
	}

	@Override
	public MfsUserProfileDTO userProfile(String username, String fhuToken) {
		
		return userProfileMock.stream().filter(item -> item.getAccountName().equalsIgnoreCase(username)).findFirst().get();
	}
	
	public static List<MfsUserProfileDTO> userProfileMock = new ArrayList<>(Arrays.asList(
			MfsUserProfileDTO.builder()
								.employeeId("00137804")
								.accountName("PhongNV5")
								.fullName("Nguyễn Văn Phong")
								.email("phongnv5@fsoft.com.vn")
								.dob(LocalDate.parse("11/11/1990", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.nationality("Viet Nam")
								.gender("Male")
								.maritalStatus("Married")
								.socialInsuranceNo("01130631990")
								.nationalId("012813349")
								.tpBankAccount("01934385201")
								.currentAddress("Cổ Nhuế 1, Bắc Từ Liêm, Hà Nội")
								.permanentAddress("Cổ Nhuế 1, Bắc Từ Liêm, Hà Nội")
								.phoneNumber("0979647699")
								.emergencyContactAddress("Hà Nội")
								.emergencyContactPhone("0866668888")
								.parentDepart("FSU1")
								.childDepart("FSU1.Aviation.R&D")
								.organizationRelation("R&D")
								.contractType("HĐLĐ xác định thời hạn")
								.startDate(LocalDate.parse("01/01/2017", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.endDate(LocalDate.parse("01/01/2019", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.jobFamily("DEV")
								.jobRank("DEV 04")
								.officerCode("Nhân viên")
								.build(),
			 MfsUserProfileDTO.builder()
								.employeeId("00086624")
								.accountName("ThaoNH2")
								.fullName("Nguyễn Hữu Thảo")
								.email("thaonh2@fsoft.com.vn")
								.dob(LocalDate.parse("11/03/1994", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.nationality("Viet Nam")
								.gender("Male")
								.maritalStatus("Single")
								.socialInsuranceNo("0116031966")
								.nationalId("013171060")
								.tpBankAccount("00080247001")
								.currentAddress("Xuân Phương, Từ Liêm, Hà Nội")
								.permanentAddress("Xuân Phương, Từ Liêm, Hà Nội")
								.phoneNumber("0387821254")
								.emergencyContactAddress("79 Cau Giay street")
								.emergencyContactPhone("0857744444")
								.parentDepart("FSU1")
								.childDepart("FSU1.BU93")
								.organizationRelation("EMP")
								.contractType("HĐLĐ xác định thời hạn")
								.startDate(LocalDate.parse("20/12/2017", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.endDate(LocalDate.parse("20/12/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.jobFamily("BANE")
								.jobRank("BANE")
								.officerCode("Nhân viên")
								.build(),
				MfsUserProfileDTO.builder()
								.employeeId("00086265")
								.accountName("HaH4")
								.fullName("Hoàng Hà")
								.email("hah4@fsoft.com.vn")
								.dob(LocalDate.parse("02/03/1988", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.nationality("Viet Nam")
								.gender("Female")
								.maritalStatus("Single")
								.socialInsuranceNo("0555031777")
								.nationalId("013171062")
								.tpBankAccount("00080247012")
								.currentAddress("Duy Tân, Cầu Giấy, Hà Nội")
								.permanentAddress("Duy Tân, Cầu Giấy, Hà Nội")
								.phoneNumber("0387821963")
								.emergencyContactAddress("79 Cau Giay street")
								.emergencyContactPhone("0857744444")
								.parentDepart("FSU1")
								.childDepart("FSU1.BU93")
								.organizationRelation("EMP")
								.contractType("HĐLĐ xác định thời hạn")
								.startDate(LocalDate.parse("20/12/2017", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.endDate(LocalDate.parse("20/12/2018", DateTimeFormatter.ofPattern("dd/MM/yyyy")))
								.jobFamily("BANE")
								.jobRank("BANE")
								.officerCode("Nhân viên")
								.build()
		));

}
