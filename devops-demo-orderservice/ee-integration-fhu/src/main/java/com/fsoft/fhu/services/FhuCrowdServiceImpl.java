package com.fsoft.fhu.services;

import java.util.Base64;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.common.utils.CommonUtils;
import com.fsoft.fhu.constants.DataConstants;
import com.fsoft.fhu.constants.FhuResponseCodeConstant;
import com.fsoft.fhu.models.MfsAuthenDTO;
import com.fsoft.fhu.models.MfsUserProfileDTO;
import com.fsoft.fhu.utils.YAMLConfig;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Service
@Qualifier("fhuCrowdService")
public class FhuCrowdServiceImpl implements FhuCrowdService {
	private static Logger logger = LoggerFactory.getLogger(FhuCrowdServiceImpl.class);

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	FhuCrowdErrorHandler fhuCrowdErrorHandler;
	
	@Autowired
	YAMLConfig yamlConfig;

//	private String applicationAuthenUrl = "https://crowd-dev.fsoft.com.vn/crowd/rest/usermanagement/latest/user?username=phongnv5";
//	private String userAuthenUrl = "https://crowd-dev.fsoft.com.vn/crowd/rest/usermanagement/1/authentication?username=";

	@Override
	public void applicationAuthenticate(String application, String password) {

		String notEncoded = application + ":" + password;
	    String encodedAuth = "Basic " + Base64.getEncoder().encodeToString(notEncoded.getBytes());
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.add("Authorization", encodedAuth);

		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		
//		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("myfsoft2", "myfsoft2"));
		
//		System.out.println(restTemplate.exchange(applicationAuthenUrl, HttpMethod.GET, entity, Object.class));
		System.out.println(entity);
		System.out.println(headers);

	}

	@Override
	public MfsAuthenDTO userAuthentication(String username, String password) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		String notEncoded = yamlConfig.getCrowd().getApplication() + ":" + yamlConfig.getCrowd().getPassword();
	    String encodedAuth = String.format("%s %s", DataConstants.HeaderConst.BASIC, Base64.getEncoder().encodeToString(notEncoded.getBytes()));
	    headers.add(DataConstants.HeaderConst.AUTHORIZATION, encodedAuth);

		UserAuthenRequest request = UserAuthenRequest.builder().value(password).build();
		HttpEntity<UserAuthenRequest> entity = new HttpEntity<UserAuthenRequest>(request, headers);

		String endpoint = String.format("%s%s", yamlConfig.getCrowd().getUserAuthenUrl(), username);
		
		ResponseEntity<String> responseData = null;

		MfsAuthenDTO response = new MfsAuthenDTO();
		restTemplate.setErrorHandler(fhuCrowdErrorHandler);
		try {
			responseData = restTemplate.exchange(endpoint, HttpMethod.POST, entity, new ParameterizedTypeReference<String>() {
			});
			
		} catch (Exception e) {
			throw new CoreServerRuntimeException(FhuResponseCodeConstant.UNKNOWN_EXCEPTION, e);
		}
		
		if(responseData == null || responseData.getBody() == null) {
			throw new CoreServerRuntimeException(FhuResponseCodeConstant.RESPONSE_NULL);
		}
		
		if(responseData.getStatusCode() != HttpStatus.OK && responseData.getStatusCode() == HttpStatus.BAD_REQUEST) {
			CrowdError error = CommonUtils.xmlToObject(responseData.getBody(), CrowdError.class);
			
			throw new CoreServerRuntimeException(FhuResponseCodeConstant.USER_OR_PASSWORD_INVALID, error.toString());
		}

		
		response = CommonUtils.xmlToObject(responseData.getBody(), MfsAuthenDTO.class);
		
		return response;
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	@Builder
	public static class UserAuthenRequest{
		private String value;
	}
	
	@XmlRootElement(name = "error")
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ToString
	@Data
	public static class CrowdError {
		@XmlElement(name = "reason")
		@JsonProperty("reason")
		private String reason;

		@XmlElement(name = "message")
		@JsonProperty("message")
		private String message;
	}
	
	@XmlRootElement(name = "user")
	@JsonIgnoreProperties(ignoreUnknown = true)
	@ToString
	@Data
	public static class UserFhu {
		 Link link;
		 @XmlElement(name = "first-name")
		 @JsonProperty("first-name")
		 private String firstName;
		 
		 @XmlElement(name = "last-name")
		 @JsonProperty("last-name")
		 private String lastName;
		 
		 @XmlElement(name = "display-name")
		 @JsonProperty("display-name")
		 private String displayName;
		 
		 @XmlElement(name = "email")
		 public String email;
		 
		 @XmlElement(name = "password")
		 public Password password;
		 
		 @XmlElement(name = "key")
		 public String key;
		 
		 @XmlElement(name = "active")
		 public String active;
		 
//		 @XmlElement(name = "attributes")
//		 public Attributes attributes;
		 @XmlElement(name = "expand")
		 public String expand;
		 @XmlElement(name = "name")
		 public String name;
	}

	@Data
	@ToString
	public static class Link
	{
	    private String rel;

	    private String href;
	}
	
	@Data
	@ToString
	public static class Password
	{
	    private Link link;
	}
	
	@Data
	@ToString
	public static class Attributes
	{
	    private Link link;

	    private Attribute[] attribute;

	}
	
	@Data
	@ToString
	public static class Attribute
	{
	    private Values values;

	    private String name;

	    private Link link;

	}
	
	@Data
	@ToString
	public static class Values
	{
	    private String[] value;

	}

	@Override
	public MfsUserProfileDTO userProfile(String username, String fhuToken) {
		// TODO Auto-generated method stub
		return null;
	}

}
