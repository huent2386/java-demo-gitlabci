package com.fsoft.fhu.services;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.fhu.constants.FhuResponseCodeConstant;
import com.fsoft.fhu.models.AuthenFsoftRequest;
import com.fsoft.fhu.models.AuthenFsoftResponse;
import com.fsoft.fhu.models.MfsAuthenDTO;
import com.fsoft.fhu.utils.YAMLConfig;

@Service
public class MfsAuthenticationServiceImpl{

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	YAMLConfig yamlConfig;
	
	public MfsAuthenDTO authen(AuthenFsoftRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//		headers.set("Authorization", "Bearer " + token);
		headers.set("ClientID", "8eedb09da9c8d279a9d304f3d602ec1d");
		String loginUrl = yamlConfig.getLogin().getFhuLoginUrl();
		HttpEntity<AuthenFsoftRequest> entity = new HttpEntity<>(request, headers);

		ResponseEntity<AuthenFsoftResponse> response = null;
		try {
			
			response = restTemplate.exchange(loginUrl, HttpMethod.POST, entity,   
					new ParameterizedTypeReference<AuthenFsoftResponse>() {
			});
		}catch (Exception e) {
			throw new CoreServerRuntimeException(FhuResponseCodeConstant.UNKNOWN_EXCEPTION.getValue(), 
					FhuResponseCodeConstant.UNKNOWN_EXCEPTION.getDisplay(), e);
		} 
		
		if(response == null || response.getBody() == null) {
			throw new CoreServerRuntimeException(FhuResponseCodeConstant.RESPONSE_NULL);
		}
		
		if (!response.getBody().getCode().equals("200")) {
			throw new CoreServerRuntimeException(response.getBody().getCode(), response.getBody().getMsg());
		}
		
		return response.getBody().getResult();
	}

}
