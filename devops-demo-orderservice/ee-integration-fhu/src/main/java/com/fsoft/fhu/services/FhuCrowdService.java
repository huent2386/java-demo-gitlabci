package com.fsoft.fhu.services;

import com.fsoft.fhu.models.MfsAuthenDTO;
import com.fsoft.fhu.models.MfsUserProfileDTO;

public interface FhuCrowdService {
	public void applicationAuthenticate(String application, String password);
	
	public MfsAuthenDTO userAuthentication(String username, String password);
	
	public MfsUserProfileDTO userProfile(String username, String fhuToken);
}
