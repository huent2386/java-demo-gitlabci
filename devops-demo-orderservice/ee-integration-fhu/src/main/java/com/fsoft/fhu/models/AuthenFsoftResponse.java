package com.fsoft.fhu.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenFsoftResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String code;
	private String msg;
	private MfsAuthenDTO result;
}
