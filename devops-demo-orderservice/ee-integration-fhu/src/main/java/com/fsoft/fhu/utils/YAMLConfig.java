package com.fsoft.fhu.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "mfs")
public class YAMLConfig {

	private Login login;

	private String anything;

	private CrowdConfig crowd;
	
	@Data
	public static class Login {
		private String fhuLoginUrl;
	}
	
	@Data
	public static class CrowdConfig {
		private String application;
		private String password;
		private String userSessionUrl;
		private String userAuthenUrl;
		private String userInfoUrl;
	}
}
