package com.fsoft.fhu.services;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

@Component
public class FhuCrowdErrorHandler implements ResponseErrorHandler {

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
//		System.out.println("hasError" + response);
		return false;
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
//		System.out.println("handleError: " + response);
	}

}
