package com.fsoft.fhu.constants;

import com.fsoft.common.models.ErrorCodeConstant;

public enum FhuResponseCodeConstant implements ErrorCodeConstant {
	EXCEPTION("FHU-3001", ""),
	DATA_INVALID("FHU-3002", "Data invalid (FHU-3002)"),
	CLIENT_ID_INVALID("FHU-3003", "ClientID invalid (FHU-3003)"),
	TOKEN_INVALID("FHU-3004", "Token invalid (FHU-3004)"),
	PERMISSION_DENIED("FHU-3005", "Permission denied (FHU-3005)"),
	USER_NOT_FOUND("FHU-3006", "User not found (FHU-3006)"),
	USER_EXPIRED("FHU-3007", "User is expired (FHU-3007)"),
	SVC_CODE_NOT_FOUND("FHU-3008", "Service not found (FHU-3008)"),
	RESPONSE_NULL("FHU-3009", "Response null (FHU-3009)"),
	USER_OR_PASSWORD_INVALID("FHU-3010", "Username or password invalid (FHU-3010)"),
	
	UNKNOWN_EXCEPTION("FHU-4001", "System Error!!! (FHU-4001)");

	private String value;
	private String display;
	FhuResponseCodeConstant(String value, String display) {
		this.value = value;
		this.display = display;
	}
	@Override
	public String getValue() {
		return value;
	}
	@Override
	public String getDisplay() {
		return display;
	}
	
}
