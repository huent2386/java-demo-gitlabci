package com.fsoft.fhu.constants;

public interface DataConstants {
	public interface HeaderConst{
		public static final String AUTHORIZATION = "Authorization";
		public static final String BASIC = "Basic";
	}
}
