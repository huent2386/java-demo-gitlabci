package com.fsoft.fhu.models;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MfsUserProfileDTO {
	private String employeeId;
	private String accountName;
	private String fullName;
	private String email;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private LocalDate dob;
	private String nationality;
	private String gender;
	private String maritalStatus;
	private String socialInsuranceNo;
	private String nationalId;
	private String tpBankAccount;
	private String currentAddress;
	private String permanentAddress;
	private String phoneNumber;
	private String emergencyContactAddress;
	private String emergencyContactPhone;
	private String parentDepart;
	private String childDepart;
	private String organizationRelation;
	private String contractType;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private LocalDate startDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private LocalDate endDate;
	private String jobFamily;
	private String jobRank;
	private String officerCode;
}
