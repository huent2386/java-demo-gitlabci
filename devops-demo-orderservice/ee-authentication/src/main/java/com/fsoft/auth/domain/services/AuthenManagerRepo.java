package com.fsoft.auth.domain.services;

import com.fsoft.auth.models.AuthenManagerInfo;

public interface AuthenManagerRepo {
	AuthenManagerInfo findByClientId(String clientId);
}
