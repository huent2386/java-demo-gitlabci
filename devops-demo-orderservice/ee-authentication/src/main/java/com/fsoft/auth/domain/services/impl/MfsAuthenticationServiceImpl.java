package com.fsoft.auth.domain.services.impl;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fsoft.auth.constants.AuthResponseCodeConstant;
import com.fsoft.auth.constants.DataConstants;
import com.fsoft.auth.domain.services.E2AuthenticationService;
import com.fsoft.auth.domain.services.LookupService;
import com.fsoft.auth.models.AuthenFsoftRequest;
import com.fsoft.auth.models.AuthenFsoftResponse;
import com.fsoft.auth.models.MfsAuthenDTO;
import com.fsoft.auth.models.UserFhuInfo;
import com.fsoft.auth.services.BaseService;
import com.fsoft.auth.utils.YAMLConfig;
import com.fsoft.common.models.CoreServerRuntimeException;

@Service
public class MfsAuthenticationServiceImpl extends BaseService implements E2AuthenticationService {

	@Autowired
	LookupService lookupService;
	
	@Autowired
	YAMLConfig yamlConfig;
	
	@Override
	public MfsAuthenDTO authen(AuthenFsoftRequest request) {
		String authenSvc = yamlConfig.getAuthenIntegration().getEndPointSvc();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//		headers.set("Authorization", "Bearer " + token);
//		headers.set("ClientID", "8eedb09da9c8d279a9d304f3d602ec1d");
		String clientId = httpRequest.getHeader(DataConstants.HeaderConst.CLIENT_ID);
		clientId = StringUtils.isEmpty(clientId) ? "A": clientId;
		String loginUrl = lookupService.endpointAuthenSvc(clientId, authenSvc) + yamlConfig.getAuthenIntegration().getEndPointApi();
		HttpEntity<AuthenFsoftRequest> entity = new HttpEntity<>(request, headers);

		ResponseEntity<AuthenFsoftResponse> response = null;
		try {
			response = restTemplate.exchange(loginUrl, HttpMethod.POST, entity,   
					new ParameterizedTypeReference<AuthenFsoftResponse>() {
			});
		}catch (Exception e) {
			throw new CoreServerRuntimeException(AuthResponseCodeConstant.UNKNOWN_EXCEPTION.getValue(), 
					AuthResponseCodeConstant.UNKNOWN_EXCEPTION.getDisplay(), e);
		} 
		
		if(response == null || response.getBody() == null) {
			throw new CoreServerRuntimeException(AuthResponseCodeConstant.RESPONSE_NULL);
		}
		
		if (!response.getBody().getCode().equals("200")) {
			throw new CoreServerRuntimeException(response.getBody().getCode(), response.getBody().getMessage());
		}
		
		return response.getBody().getData();
	}

}
