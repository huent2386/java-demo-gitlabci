package com.fsoft.auth.models;

import java.io.Serializable;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class MfsAuthenDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String accessToken;
	private String username;
	private String firstName;
	private String lastName;
	private String phone;
	private String displayName;
	private String email;
	@JsonIgnore
	private String name;
	
	private String fhuToken;
	
	public String getUsername() {
		return StringUtils.isEmpty(username) ? name : username;
	}
	
}