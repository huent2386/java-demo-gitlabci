package com.fsoft.auth.domain.services.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.fsoft.auth.domain.services.LoginService;
import com.fsoft.auth.domain.services.UserRepo;
import com.fsoft.auth.models.MfsAuthenDTO;
import com.fsoft.auth.models.request.LoginRequest;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepo userRepo;
	
	@Override
	public MfsAuthenDTO login(LoginRequest request) {
		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(),
						request.getPassword()));
		
		// Inject into security context
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        return ((MfsAuthenDTO) authentication.getDetails());
	}

	private void syncAccount(String username) {
//		UserEntity user = UserEntity.builder()
//									.username(username)
//									.enabled(true)
//									.build();
//		
//		userRepo.save(user);
	}
}