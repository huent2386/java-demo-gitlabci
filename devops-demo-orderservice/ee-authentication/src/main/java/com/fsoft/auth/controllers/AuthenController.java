package com.fsoft.auth.controllers;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fsoft.auth.models.MfsAuthenDTO;
import com.fsoft.auth.models.request.LoginRequest;
import com.fsoft.auth.models.request.RegisterRequest;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/auth")
public interface AuthenController {
	
	@PostMapping("/register")
	public ResponseEntity<ResponseData<String>> register(@Valid @RequestBody RegisterRequest request);
	
	@PostMapping("/login")
	public ResponseEntity<ResponseData<MfsAuthenDTO>> authentication(@Valid @RequestBody LoginRequest request);
	
	@GetMapping("/ping")
	public ResponseEntity<ResponseData<String>> ping();
}
