package com.fsoft.auth.models.request;

import javax.validation.constraints.NotNull;

import com.fsoft.common.utils.CommonUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
public class LoginRequest extends BaseRequest {
	@NotNull(message = "Username is required")
	private String username;
	
	@NotNull(message = "Password is required")
	private String password;
	
	@Override
	public String toString() {
		return CommonUtils.toJsonString(this);
	}
}
