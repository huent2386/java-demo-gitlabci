package com.fsoft.auth.models;

import java.util.UUID;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserDetailExtend extends UserDetails {
	UUID getUserId();
}
