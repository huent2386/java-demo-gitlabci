package com.fsoft.auth.services;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import com.fsoft.auth.models.UserDetailExtend;

public class TokenBasedAuthentication extends AbstractAuthenticationToken {
	private static final long serialVersionUID = 1L;
	private String token;
	private final UserDetailExtend principle;

	public TokenBasedAuthentication(UserDetailExtend principle) {
		super(principle.getAuthorities());
		this.principle = principle;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public boolean isAuthenticated() {
		return true;
	}

	@Override
	public Object getCredentials() {
		return token;
	}

	@Override
	public UserDetailExtend getPrincipal() {
		return principle;
	}


}
