package com.fsoft.auth.logger;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class LoggingAspect extends LoggingPointcut {
	private static Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

	@Autowired
	HttpServletRequest request;

//	@Around("execution(* game.auth.server.service.*.*(..))")
//	public Object userAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
//		logger.info(new StringBuffer("[SERVICE-START] ").append(joinPoint.getSignature().getDeclaringTypeName())
//				.append(".").append(joinPoint.getSignature().getName()).toString());
//
//		Long t1 = System.currentTimeMillis();
//		Object value = joinPoint.proceed();
//
//		logger.info("[SERVICE-END]: " + joinPoint.getSignature().getDeclaringTypeName() + " : "
//				+ (System.currentTimeMillis() - t1));
//		return value;
//	}

	@Around("restcontroller()")
	public Object userAdviceController(ProceedingJoinPoint joinPoint) throws Throwable {
		try {
			logger.info(
					"\n===========================================================================================\n"
							+ "\n===========================================================================================\n");
			logger.info(new StringBuffer("[CONTROLLER-START] ").append(joinPoint.getSignature().getDeclaringTypeName())
					.append(".").append(joinPoint.getSignature().getName()).toString());
			MethodSignature method = (MethodSignature) joinPoint.getStaticPart().getSignature();
			List<String> params = Arrays.asList(method.getParameterNames());
			StringBuffer requestUrl = new StringBuffer(" RequestURL -->  ").append(request.getRequestURL());
			List<Object> args = Arrays.asList(joinPoint.getArgs());
			if (!StringUtils.isEmpty(request.getQueryString())) {
				requestUrl.append("?").append(request.getQueryString());
			}
			logger.info(requestUrl.toString());

			StringBuffer str = new StringBuffer(" Parameters -->  ");
			Iterator<Object> i1 = args.iterator();
			params.stream().forEach(param -> str.append(", ").append(param).append(" = ").append(i1.next()));
			logger.info(str.toString());

			if (request.getHeaderNames() != null) {
				StringBuffer sb = new StringBuffer(" RequestHeader --> ");
				Map<String, String> map = new HashMap<String, String>();
				Enumeration<String> headerNames = request.getHeaderNames();
				while (headerNames.hasMoreElements()) {
					String key = (String) headerNames.nextElement();
					String value = request.getHeader(key);
					map.put(key, value);
					sb.append(" ,").append(key).append(":").append(value);
				}
				String ip = request.getHeader("X-FORWARDED-FOR");
				if (ip == null) {
					ip = request.getRemoteAddr();
				}

				sb.append(" ,").append("IP-Client").append(":").append(ip);
				logger.info(sb.toString());
			}
		} catch (Exception ex) {

		}

		Long t1 = System.currentTimeMillis();

		Object value = joinPoint.proceed();

		try {
			logger.info("[CONTROLLER-END]: " + joinPoint.getSignature().getDeclaringTypeName() + " : "
					+ (System.currentTimeMillis() - t1));
		} catch (Exception ex) {
		}

		return value;
	}
}
