package com.fsoft.auth.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenManagerInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String clientId;
	private String clientSecret;
	private int expirationIn;
	private String notes;
}
