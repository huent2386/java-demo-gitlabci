package com.fsoft.auth.config;

import java.util.TimeZone;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.context.annotation.Configuration;

@Configuration
public class WebAppInitializer  implements ServletContextListener  {

	private void setupTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+0"));
    }

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		setupTimeZone();
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}
	
}
