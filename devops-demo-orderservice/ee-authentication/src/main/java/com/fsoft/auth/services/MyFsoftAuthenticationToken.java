package com.fsoft.auth.services;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.fsoft.auth.models.MfsAuthenDTO;

public class MyFsoftAuthenticationToken extends UsernamePasswordAuthenticationToken{
	private static final long serialVersionUID = 1L;
	
	public MyFsoftAuthenticationToken(MfsAuthenDTO userFsoft, Authentication authentication, 
			Collection<? extends GrantedAuthority> authorities, String username) {
		super(username ,authentication.getCredentials(), authorities);
		this.setDetails(userFsoft);
	}

}
