package com.fsoft.auth.services;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fsoft.auth.models.UserDetailExtend;
import com.fsoft.auth.models.UserModel;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private ModelMapper modelMapper;

//	@Autowired
//	private AuthenticationManager authenticationManager;
	
	@Override
	public UserDetailExtend loadUserByUsername(String username) throws UsernameNotFoundException {
		UserModel user = userService.findUserInfoByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			return  modelMapper.map(user, UserModel.class);
		}
	}

	public void changePassword(String oldPassword, String newPassword) {
		Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
		String username = currentUser.getName();

//		if (authenticationManager != null) {
//			logger.debug("Re-authenticating user '" + username + "' for password change request.");
//
//			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
//		} else {
//			logger.debug("No authentication manager set. can't change Password!");
//
//			return;
//		}

		logger.debug("Changing password for user '" + username + "'");

		UserModel user = (UserModel) loadUserByUsername(username);

//		user.setPassword(passwordEncoder.encode(newPassword));
//		userService.save(user);

	}
}