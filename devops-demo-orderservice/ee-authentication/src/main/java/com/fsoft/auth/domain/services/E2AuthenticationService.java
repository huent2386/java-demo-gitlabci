package com.fsoft.auth.domain.services;

import com.fsoft.auth.models.AuthenFsoftRequest;
import com.fsoft.auth.models.MfsAuthenDTO;
import com.fsoft.auth.models.UserFhuInfo;

public interface E2AuthenticationService {
	MfsAuthenDTO authen(AuthenFsoftRequest request);
}
