package com.fsoft.auth.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "ee-authen")
public class YAMLConfig {

	private Lookup lookup;

	private String anything;
	
	private AuthenIntegration authenIntegration;

	@Data
	public static class Lookup {
		private String endPointUrl;
	}
	
	@Data
	public static class AuthenIntegration {
		private String endPointSvc;
		private String endPointApi;
	}
}
