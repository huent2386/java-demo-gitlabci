package com.fsoft.auth.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.auth.domain.services.UserRepo;
import com.fsoft.auth.models.UserModel;
import com.fsoft.redis.RedisManagerService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepo userRepo;
	
	@Autowired
	RedisManagerService redisManagerService;
	
	@Override
	public UserModel findUserInfoByUsername(String username) {
		UserModel userInfo = this.findInCache(username);
		if(userInfo != null) {
			return userInfo;
		}
		
		userInfo = cacheUserInfo(username);
		
		return userInfo;
	}
	
	private UserModel cacheUserInfo(String username) {
		UserModel user = userRepo.findByUsername(username);
//		redisManagerService.setValue(user.getUsername(), RedisCacheConstants.USER_INFO, user);
		
		return user;
	}

	private UserModel findInCache(String username) {
		return null;
//		return redisManagerService.getValue(username, RedisCacheConstants.USER_INFO, UserModel.class);
	}
}