//package com.fsoft.auth.domain.services.test;
//
//import static org.mockito.Mockito.doThrow;
//
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.ArgumentMatchers;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//
//import com.fsoft.auth.domain.services.impl.LookupServiceImpl;
//import com.fsoft.auth.models.LookupSvcResponse;
//import com.fsoft.auth.models.ServiceMapDTO;
//import com.fsoft.auth.utils.YAMLConfig;
//import com.fsoft.auth.utils.YAMLConfig.Lookup;
//import com.fsoft.common.models.CoreServerRuntimeException;
//
//@RunWith(MockitoJUnitRunner.class)
//public class LookupServiceTest {
//
//	@Mock
//	RestTemplate restTemplate;
//	
//	@Mock
//	YAMLConfig yamlConfig;
//	
//	@InjectMocks
//	LookupServiceImpl lookupService;
//	
//	@Mock
//	Lookup lookupModel;
//	
//	@Before
//	public void setUp() throws Exception {
////		lookupService = new LookupServiceImpl();
//	}
//
//	@Test
//	public void endpointAuthenSvc_Success() {
//		String clientId = "A";
//		String svcCode = "B";
//		String endpointUrl = "http://localhost:8080/map/%s/%s";
//		String svcUrl = "http://myservice";
//		Mockito.when(yamlConfig.getLookup()).thenReturn(lookupModel);
//		Mockito.when(lookupModel.getEndPointUrl()).thenReturn(endpointUrl);
//		ResponseEntity<LookupSvcResponse> response = ResponseEntity.ok()
//																.body(LookupSvcResponse.builder()
//																						.code("200")
//																						.data(ServiceMapDTO.builder().svcUrl(svcUrl).build())
//																						.build());
//		Mockito.when(restTemplate.exchange(ArgumentMatchers.eq(String.format(endpointUrl, clientId, svcCode)),
//				ArgumentMatchers.eq(HttpMethod.GET),
//				ArgumentMatchers.<HttpEntity<Void>>any(),
//				ArgumentMatchers.<ParameterizedTypeReference<LookupSvcResponse>>any())
//				).thenReturn(response);
//		
//		String svcUrlTest = lookupService.endpointAuthenSvc(clientId, svcCode);
//		System.out.println(svcUrlTest);
//		Assert.assertEquals(svcUrl, svcUrlTest);
//		
//	}
//	
//	@Test(expected = Exception.class)
//	public void endpointAuthenSvc_ThrowException() {
//		String clientId = "A";
//		String svcCode = "B";
//		String endpointUrl = "http://localhost:8080/map/%s/%s";
//		String svcUrl = "http://myservice";
//		Mockito.when(yamlConfig.getLookup()).thenReturn(lookupModel);
//		Mockito.when(lookupModel.getEndPointUrl()).thenReturn(endpointUrl);
//		ResponseEntity<LookupSvcResponse> response = ResponseEntity.ok()
//																.body(LookupSvcResponse.builder()
//																						.code("3002")
//																						.data(ServiceMapDTO.builder().svcUrl(svcUrl).build())
//																						.build());
//		Mockito.when(restTemplate.exchange(ArgumentMatchers.eq(String.format(endpointUrl, clientId, svcCode)),
//				ArgumentMatchers.eq(HttpMethod.GET),
//				ArgumentMatchers.<HttpEntity<Void>>any(),
//				ArgumentMatchers.<ParameterizedTypeReference<LookupSvcResponse>>any())
//				).thenThrow(CoreServerRuntimeException.class);
//		
//		String svcUrlTest = lookupService.endpointAuthenSvc(clientId, svcCode);
//		Assert.assertEquals(svcUrl, svcUrlTest);
//		
//	}
//
//}
