package com.fsoft.servicediscovery.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class ServiceManagerDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String clientId;
	private String clientSecret;
	private List<String> svcCodes;
	private long expirationIn;
	private String notes;
	private int status;
}
