package com.fsoft.servicediscovery.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "ServiceManagers", indexes = { 
		@Index(columnList = "Id", name = "idx_servicemanager_id"),
		@Index(columnList = "ClientId", name = "idx_servicemanager_clientid"),
		@Index(columnList = "SvcCodes", name = "idx_servicemanager_svccodes"),
		@Index(columnList = "ExpirationDate", name = "idx_servicemanager_expirationdate"),
		@Index(columnList = "CreatedTime", name = "idx_servicemanager_createdtime"),
		@Index(columnList = "CreatedDate", name = "idx_servicemanager_createddate")})
@DynamicUpdate
public class ServiceManagerEntity {
	@Id
	@Column(name = "Id", unique = true, updatable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "ClientId", unique = true, updatable = false, nullable = false)
	private String clientId;

	@Column(name = "ClientSecret", nullable = false)
	private String clientSecret;

	@Column(name = "SvcCodes", nullable = false)
	private String svcCodes;
	
	@Column(name = "ExpirationIn", nullable = false)
	private long expirationIn;

	@Column(name = "Notes")
	private String notes;

	@Column(name = "Status", nullable = false)
	private int status;
	
	@Column(name = "ExpirationDate", nullable = false)
	private LocalDate expirationDate;
	
	@Column(name = "CreatedDate")
	@Builder.Default
	private LocalDate createdDate = LocalDate.now();
	
	@Column(name = "CreatedTime", nullable = false, columnDefinition = "timestamp")
	@Builder.Default
	private LocalDateTime createdTime = LocalDateTime.now();
	
	@Column(name = "UpdatedTime", nullable = false, columnDefinition = "timestamp")
	@Builder.Default
	private LocalDateTime updatedTime = LocalDateTime.now();
}
