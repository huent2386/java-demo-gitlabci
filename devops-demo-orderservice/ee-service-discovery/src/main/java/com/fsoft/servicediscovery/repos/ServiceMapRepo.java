package com.fsoft.servicediscovery.repos;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.servicediscovery.entities.ServiceMapEntity;

@Repository
public interface ServiceMapRepo extends JpaRepository<ServiceMapEntity, UUID>{
	
	@Query("SELECT a FROM ServiceMapEntity a WHERE a.svcCode = :svcCode")
	ServiceMapEntity findBySvcCode(@Param("svcCode") String svcCode);
	
	@Query("SELECT a FROM ServiceMapEntity a WHERE a.clientId = :clientId AND a.svcCode = :svcCode")
	ServiceMapEntity findByClientIdAndSvcCode(@Param("clientId") String clientId, @Param("svcCode") String svcCode);
}
