package com.fsoft.servicediscovery.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fsoft.common.models.ResponseData;
import com.fsoft.servicediscovery.entities.TestItem;
import com.fsoft.servicediscovery.models.ServiceManagerDTO;
import com.fsoft.servicediscovery.models.ServiceMapDTO;

@RequestMapping("/discovery/")
public interface ServiceDiscoveryController {

	@GetMapping("/svc-manager/{clientId}")
	public ResponseEntity<ResponseData<ServiceManagerDTO>> findByClientId(@PathVariable String clientId);
	
	@PostMapping("/svc-manager")
	public ResponseEntity<ResponseData<String>> saveSvcManager(@Valid @RequestBody ServiceManagerDTO request);
	
	@GetMapping("/svc-map/{svcCode}")
	public ResponseEntity<ResponseData<ServiceMapDTO>> findBySvcCode(@PathVariable String svcCode);
	
	@PostMapping("/svc")
	public ResponseEntity<ResponseData<String>> sdfsd(@Valid @RequestBody TestItem request);
	
	@GetMapping("/svc-map/{clientId}/{svcCode}")
	public ResponseEntity<ResponseData<ServiceMapDTO>> findByClientIdAndSvcCode(
			@PathVariable("clientId") String clientId, @PathVariable("svcCode") String svcCode);
}
