package com.fsoft.servicediscovery.repos;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.fsoft.servicediscovery.entities.TestItem;

@Repository
public class TestItemCustom {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void insertDb(TestItem data) {
		
		jdbcTemplate.update("Insert into TestItem(id, nickname, user_id, username) values(?, ?, ?, ?);",
                data.getId(), data.getNickname(), data.getUserId(), data.getUsername());
	}
}
