package com.fsoft.servicediscovery.repos;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.servicediscovery.entities.ServiceManagerEntity;

@Repository
public interface ServiceManagerRepo extends JpaRepository<ServiceManagerEntity, UUID> {

	@Query("SELECT a FROM ServiceManagerEntity a WHERE a.clientId = :clientId")
	ServiceManagerEntity findByClientId(@Param("clientId") String clientId);
}
