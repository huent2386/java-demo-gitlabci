package com.fsoft.servicediscovery.controller;

import java.util.Arrays;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.common.models.ResponseData;
import com.fsoft.servicediscovery.constants.DiscoveryCodeConstant;
import com.fsoft.servicediscovery.entities.ServiceManagerEntity;
import com.fsoft.servicediscovery.entities.ServiceMapEntity;
import com.fsoft.servicediscovery.entities.TestItem;
import com.fsoft.servicediscovery.models.ServiceManagerDTO;
import com.fsoft.servicediscovery.models.ServiceMapDTO;
import com.fsoft.servicediscovery.repos.ServiceManagerRepo;
import com.fsoft.servicediscovery.repos.ServiceMapRepo;
import com.fsoft.servicediscovery.repos.TestItemCustom;
import com.fsoft.servicediscovery.repos.TestItemRepos;

@RestController
public class ServiceDiscoveryControllerImpl implements ServiceDiscoveryController {

	@Autowired
	ServiceManagerRepo serviceManagerRepo;

	@Autowired
	ServiceMapRepo serviceMapRepo;
	
	@Autowired
	TestItemRepos testItemRepo;
	
	@Autowired
	TestItemCustom testItemCustom;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public ResponseEntity<ResponseData<ServiceManagerDTO>> findByClientId(@PathVariable String clientId) {
		ServiceManagerEntity dataEntity = serviceManagerRepo.findByClientId(clientId);
		
		ServiceManagerDTO svcManagerDTO = modelMapper.map(dataEntity, ServiceManagerDTO.class);
		svcManagerDTO.setSvcCodes(Arrays.asList(dataEntity.getSvcCodes().split(";")));
		
		return ResponseEntity.ok(ResponseData.createSuccess(svcManagerDTO));
	}

	@Override
	public ResponseEntity<ResponseData<String>> saveSvcManager(@Valid @RequestBody ServiceManagerDTO request) {
		ServiceManagerEntity data = ServiceManagerEntity.builder()
												.clientId(request.getClientId())
												.clientSecret(request.getClientSecret())
												.expirationIn(1234567)
												.svcCodes("EE_SVC;AUTH_SVC")
												.notes("")
												.status(1)
												.build();
		
		serviceManagerRepo.save(data);
		return ResponseEntity.ok(ResponseData.createSuccess("Success"));
	}

	@Override
	public ResponseEntity<ResponseData<ServiceMapDTO>> findBySvcCode(@PathVariable String svcCode) {
		ServiceMapEntity dataEntity = serviceMapRepo.findBySvcCode(svcCode);
		
		return ResponseEntity.ok(ResponseData.createSuccess(modelMapper.map(dataEntity, ServiceMapDTO.class)));
	}

	@Override
	public ResponseEntity<ResponseData<String>> sdfsd(@Valid @RequestBody TestItem request) {
//		testItemRepo.save(request);
		if(request.getId() == null) {
			testItemRepo.save(request);
		}else {
//			testItemRepo.insertData(request.getNickname(), request.getUserId(), request.getUsername(), request.getId());
			testItemCustom.insertDb(request);
		}
		
		return ResponseEntity.ok(ResponseData.createSuccess("Success"));
	}

	@Override
	public ResponseEntity<ResponseData<ServiceMapDTO>> findByClientIdAndSvcCode(
			@PathVariable("clientId") String clientId, @PathVariable("svcCode") String svcCode) {

		ServiceMapEntity response = serviceMapRepo.findByClientIdAndSvcCode(clientId, svcCode);
//		if(response == null) {
//			throw new CoreServerRuntimeException(DiscoveryCodeConstant.SVC_CODE_NOT_FOUND);
//		}
		return ResponseEntity.ok(ResponseData.createSuccess(response == null ? null : modelMapper.map(response, ServiceMapDTO.class)));
	}

	
}
