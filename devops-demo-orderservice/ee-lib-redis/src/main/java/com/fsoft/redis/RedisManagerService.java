package com.fsoft.redis;

import java.util.Map;

public interface RedisManagerService {
	public void deleteKey(String key);

	public <T> T getValue(final String key, Class<T> clzz);

	public void setValue(final String key, final Object value, long timeout);

	public void setValue(final String key, final Object value);
	
	public <T> T getValue(String groupName, final String key, Class<T> clzz);
	
	public Map<Object, Object> getValue(String groupName);
	
	public boolean isExistedHashKey(String groupName);
	
	public boolean isExistedUser(String username);
	
	public void setValue(String groupName, String key, Object value, long timeout);
	
	public void setValue(String groupName, Map<Object, Object> map, long timeout);
	
	public void setValue(String groupName, String key, Object value);
}
