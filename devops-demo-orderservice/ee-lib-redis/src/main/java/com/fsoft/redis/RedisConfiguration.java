package com.fsoft.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

@Configuration
public class RedisConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(RedisConfiguration.class);
	
	@Value("${spring.redis.host}")
    private String redisHostName;
	

    @Value("${spring.redis.port}")
    private int redisPort;
	
	@Bean
	public RedisConnectionFactory jedisConnectionFactory() {
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(500);
		poolConfig.setMinIdle(2);
		poolConfig.setMaxIdle(500);
		poolConfig.setMaxWaitMillis(3000);

		JedisConnectionFactory factory = new JedisConnectionFactory(poolConfig);
		factory.setUsePool(true);
		factory.setHostName(redisHostName);
		factory.setUsePool(true);
		factory.setPort(redisPort);
		factory.setTimeout(Protocol.DEFAULT_TIMEOUT);
		
		logger.info(String.format("=========== Start Redis: %s %d", factory.getHostName(), factory.getPort()));
		return  factory;
	}

//	@Bean(name = "redisTemplate")
//	public RedisTemplate<String, String> redisTemplate() {
//		RedisTemplate<String, String> template = new RedisTemplate<>();
//		template.setConnectionFactory(this.jedisConnectionFactory());
//		template.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
//		template.setKeySerializer(new StringRedisSerializer());
//		template.setHashKeySerializer(new GenericJackson2JsonRedisSerializer());
//		template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//		template.setEnableTransactionSupport(true);
//		
//		return template;
//	}
	
	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		RedisTemplate<String, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(this.jedisConnectionFactory());
		template.setKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
		// template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
		 template.setHashKeySerializer(template.getKeySerializer());
		 template.setHashValueSerializer(template.getValueSerializer());

//		template.setEnableTransactionSupport(true);
		return template;
	}
}
