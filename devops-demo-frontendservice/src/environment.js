(function (window) {
    window.environment = window.environment || {};

    switch (window.location.hostname) {
        case 'admin.demo.akawork.io':
            // API URL Service
            window.environment.apiURL = 'https://api.demo.akawork.io/demo/orderservice/api/v1/order';
            window.environment.urlGetProduct = 'https://api.demo.akawork.io/demo/productservice/api/v1/products';
            window.environment.urlOrder = 'https://api.demo.akawork.io/demo/orderservice/api/v1/orders';
            break;
        case 'test.demo.akawork.io':
            // API URL Service
            window.environment.apiURL = 'https://api3.demo.akawork.io/demo/orderservice/api/v1/order';
            window.environment.urlGetProduct = 'https://api3.demo.akawork.io/demo/productservice/api/v1/products';
            window.environment.urlOrder = 'https://api3.demo.akawork.io/demo/orderservice/api/v1/orders';
            break;
        case 'dev.demo.akawork.io':
            // API URL Service
            window.environment.apiURL = 'https://api2.demo.akawork.io/demo/orderservice/api/v1/order';
            window.environment.urlGetProduct = 'https://api2.demo.akawork.io/demo/productservice/api/v1/products';
            window.environment.urlOrder = 'https://api2.demo.akawork.io/demo/orderservice/api/v1/orders';
            break;
        default:
            window.environment.apiURL = 'https://api.demo.akawork.io/demo/orderservice/api/v1/order';
            window.environment.urlGetProduct = 'https://api.demo.akawork.io/demo/productservice/api/v1/products';
            window.environment.urlOrder = 'https://api.demo.akawork.io/demo/orderservice/api/v1/orders';
    }
}(this));
