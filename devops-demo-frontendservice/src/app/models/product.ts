export class Product {
  id: number;
  code: string;
  name: string;
  description: string;
  price: number;
  volume: number;
}
