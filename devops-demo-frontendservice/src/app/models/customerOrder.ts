import { OrderDetail } from './orderDetail';

export class CustomerOrder {
  customerName: string;
  address: string;
  total: number;
  orderDetails: OrderDetail[];
}
