export class ProductOrderRequest {
  id: number;
  code: string;
  name: string;
  description: string;
  price: number;
  quantity: number = 1;
}
