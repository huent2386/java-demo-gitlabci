export class OrderDetail{
  productId: number;
  productCode: string;
  productName: string;
  quantity: number;
}
