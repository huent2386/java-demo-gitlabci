export class ProductRequest {
  code: string;
  name: string;
  description: string;
  price: number;
  volume: number;
}
