import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
//import { environment } from 'src/environments/environment';
import { EnvironmentService } from './environment.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  // private url = 'https://api.demo.akawork.io/demo/orderservice/api/v1/order';
  //protected environmentService: EnvironmentService;
  private url = this.environmentService.apiURL;

  constructor(private http: HttpClient, protected environmentService: EnvironmentService) { }


}
