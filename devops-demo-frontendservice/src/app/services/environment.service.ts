import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class EnvironmentService {
    public apiURL = '';
    public urlGetProduct = '';
    public urlOrder = '';
    constructor() {
    }
}
