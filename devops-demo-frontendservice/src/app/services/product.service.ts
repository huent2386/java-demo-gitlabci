import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { CustomerOrder } from '../models/customerOrder';
import { EnvironmentService } from './environment.service';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class ProductService {

  //private urlGetProduct = 'https://api.demo.akawork.io/demo/productservice/api/v1/products';
  //private urlOrder = 'https://api.demo.akawork.io/demo/orderservice/api/v1/orders';
  // protected environmentService: EnvironmentService;
  private urlGetProduct = this.environmentService.urlGetProduct;
  private urlOrder = this.environmentService.urlOrder;

  constructor(private http: HttpClient, protected environmentService: EnvironmentService) { }

  getProducts(): Observable<any> {
    return this.http.get<any>(this.urlGetProduct);
  }

  createProduct(product): Observable<Product> {
    return this.http.post<Product>(this.urlGetProduct, product);
  }

  buyProduct(customerOrder): Observable<CustomerOrder> {
    return this.http.post<CustomerOrder>(this.urlOrder, customerOrder);
  }

  getAllOrder(): Observable<any> {
    return this.http.get<any>(this.urlOrder);
  }
}
