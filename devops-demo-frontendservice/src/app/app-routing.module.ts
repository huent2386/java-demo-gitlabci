import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './components/product/product.component';
import { OrderComponent } from './components/order/order.component';
import { ProductAddComponent } from './components/product-add/product-add.component';

const routes: Routes = [
  {
    path: 'products',
    component: ProductComponent,
    data: {title: 'Product List'}
  },
  {
    path: 'order',
    component: OrderComponent,
    data: {title: 'Order Detail'}
  },
  {
    path: 'product-add',
    component: ProductAddComponent,
    data: {title: 'Product Add'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
