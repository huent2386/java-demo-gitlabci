import { ProductOrderRequest } from './../../models/productOrderRequest';
import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import {TableModule} from 'primeng/table';
import { Product } from 'src/app/models/product';
import { OrderDetail } from 'src/app/models/orderDetail';
import { CustomerOrder } from 'src/app/models/customerOrder';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  productData: [];
  buyItem: ProductOrderRequest[] = [];
  displayedColumns: string[] = ['prod_code', 'prod_name', 'prod_price', 'prod_vol', 'action'];
  isLoadingResults=false;
  orderList: CustomerOrder[] = [];
  orderDisplayColumns: string[] = ['position', 'customer_name', 'detail', 'total_amount'];

  customerOrder: CustomerOrder = new CustomerOrder();

  constructor(private svc: ProductService) { }

  ngOnInit() {
    this.isLoadingResults = true;
    this.svc.getProducts().subscribe(data => {
      this.productData = data.data;
      this.isLoadingResults = false;
      console.log(data);
    });

    this.loadAllOrder();
  }

  onSearchClick(item: Product) {
    const found = this.buyItem.find(element => {
      return item.id === element.id;
    });
    if(found){
      return;
    }
    let data: ProductOrderRequest = new ProductOrderRequest();
    this.buyItem.push(Object.assign(data, item));
  }

  onBuy() {
    this.isLoadingResults = true;
    let listOrderDetails: OrderDetail[] = [];
    let totalAmount: number = 0;
    this.buyItem.forEach(item => {
      let temp: OrderDetail = new OrderDetail();
      temp.productCode = item.code;
      temp.productId = item.id;
      temp.productName = item.name;
      temp.quantity = item.quantity;

      totalAmount += (item.price * item.quantity);

      listOrderDetails.push(temp);
    });

    this.customerOrder.orderDetails = listOrderDetails;
    this.customerOrder.address = '17 Duy Tan Street';
    this.customerOrder.customerName = 'Poop';
    this.customerOrder.total = totalAmount;

    this.svc.buyProduct(this.customerOrder).subscribe(res => {
      if(res) {
        this.svc.getProducts().subscribe(data => {
          this.productData = data.data;
          this.isLoadingResults = false;
          console.log(data);
        });
      }
      this.loadAllOrder();
      this.isLoadingResults = false;

      this.buyItem = [];
    }, (err) => {
      console.log(err);
      this.isLoadingResults = false;
    });

  }

  loadAllOrder() {
    this.svc.getAllOrder().subscribe(data => {
      this.orderList = data.data;
    });
  }

}
