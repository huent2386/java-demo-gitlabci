import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Product } from 'src/app/models/product';
import { ProductRequest } from 'src/app/models/productRequest';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {
  productData: [];

  constructor(private svc: ProductService, private router: Router, private formBuilder: FormBuilder ) { }

  productForm: FormGroup;
  prod_code: string='';
  prod_name: string='';
  prod_desc: string='';
  prod_price: number=null;
  prod_vol: number=null;
  isLoadingResults = false;
  ngOnInit() {
    this.productForm = this.formBuilder.group({
      'id': 1,
      'prod_code' : [null, Validators.required],
      'prod_name' : [null, Validators.required],
      'prod_desc' : [null, Validators.required],
      'prod_price' : [null, Validators.required],
      'prod_vol' : [null, Validators.required]
    });
  }

  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
    const product: ProductRequest = {
      code: form['prod_code'],
      name: form['prod_name'],
      description: form['prod_desc'],
      price: form['prod_price'],
      volume: form['prod_vol']
    };
    this.svc.createProduct(product)
      .subscribe(res => {
          console.log(res);
          this.isLoadingResults = false;
          this.router.navigate(['/products']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        });
  }


}
