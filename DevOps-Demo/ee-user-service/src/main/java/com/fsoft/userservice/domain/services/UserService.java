package com.fsoft.userservice.domain.services;
//
//import com.fsoft.userservice.models.MfsUserProfileDTO;
//
//public interface UserService {
//	MfsUserProfileDTO userProfile();
//}

import com.fsoft.userservice.entity.UserEntity;
import com.fsoft.userservice.logger.CreateUserRequest;

public interface UserService {
	UserEntity userProfile(Long userId);
	UserEntity createUser(CreateUserRequest request);
}