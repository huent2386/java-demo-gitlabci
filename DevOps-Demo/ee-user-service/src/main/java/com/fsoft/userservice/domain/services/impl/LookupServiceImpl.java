//package com.fsoft.userservice.domain.services.impl;
//
//import java.util.Collections;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import com.fsoft.common.models.CoreServerRuntimeException;
//import com.fsoft.userservice.constants.UserServiceResponseCodeConstant;
//import com.fsoft.userservice.domain.services.LookupService;
//import com.fsoft.userservice.models.LookupSvcResponse;
//import com.fsoft.userservice.utils.YAMLConfig;
//
//@Service
//public class LookupServiceImpl implements LookupService {
//
//	@Autowired
//	RestTemplate restTemplate;
//
//	@Autowired
//	YAMLConfig yamlConfig;
//
//	@Override
//	public void findByClientIdAndSvcCode(String clientId, String svcCode) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public String endpointAuthenSvc(String clientId, String svcCode) {
//		String lookupEndpoint = yamlConfig.getLookup().getEndPointUrl();
//
//		lookupEndpoint = String.format(lookupEndpoint, clientId, svcCode);
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//		HttpEntity<Void> entity = new HttpEntity<>(headers);
//
//		ResponseEntity<LookupSvcResponse> response = null;
//		try {
//
//			response = restTemplate.exchange(lookupEndpoint, HttpMethod.GET, entity,
//					new ParameterizedTypeReference<LookupSvcResponse>() {
//			});
//		}catch (Exception e) {
//			throw new CoreServerRuntimeException(UserServiceResponseCodeConstant.UNKNOWN_EXCEPTION.getValue(),
//					UserServiceResponseCodeConstant.UNKNOWN_EXCEPTION.getDisplay(), e);
//		}
//
//		if(response == null || response.getBody() == null) {
//			throw new CoreServerRuntimeException(UserServiceResponseCodeConstant.RESPONSE_NULL);
//		}
//
//		if (!response.getBody().getCode().equals("200")) {
//			throw new CoreServerRuntimeException(response.getBody().getCode(), response.getBody().getMessage());
//		}
//
//		return response.getBody().getData().getSvcUrl();
//	}
//
//}
