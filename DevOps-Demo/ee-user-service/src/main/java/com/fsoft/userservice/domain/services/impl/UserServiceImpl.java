package com.fsoft.userservice.domain.services.impl;

import com.fsoft.userservice.domain.services.UserService;
import com.fsoft.userservice.entity.UserEntity;
import com.fsoft.userservice.logger.CreateUserRequest;
import com.fsoft.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//
//import java.util.Collections;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//
//import com.fsoft.common.models.CoreServerRuntimeException;
//import com.fsoft.redis.RedisManagerService;
//import com.fsoft.userservice.config.ConversationState;
//import com.fsoft.userservice.constants.DataConstants;
//import com.fsoft.userservice.constants.UserServiceResponseCodeConstant;
//import com.fsoft.userservice.domain.services.LookupService;
//import com.fsoft.userservice.domain.services.UserService;
//import com.fsoft.userservice.models.MfsUserProfileDTO;
//import com.fsoft.userservice.models.MfsUserProfileResponse;
//import com.fsoft.userservice.utils.YAMLConfig;
//
//@Service
//public class UserServiceImpl extends BaseService implements UserService {
//
//	@Autowired
//	LookupService lookupService;
//
//	@Autowired
//	YAMLConfig yamlConfig;
//
//	@Autowired
//	RedisManagerService redisManagerService;
//
//	@Override
//	public MfsUserProfileDTO userProfile() {
//		String userProfileSvc = yamlConfig.getFhu().getUserProfileSvc();
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//		String clientId = httpRequest.getHeader(DataConstants.HeaderConst.CLIENT_ID);
//		clientId = StringUtils.isEmpty(clientId) ? "A": clientId;
//		String userProfileUrl = lookupService.endpointAuthenSvc(clientId, userProfileSvc) + yamlConfig.getFhu().getUserProfileApi();
//		HttpEntity<Void> entity = new HttpEntity<>(headers);
//
//		ResponseEntity<MfsUserProfileResponse> response = null;
//
//		String username = redisManagerService.getValue(ConversationState.getAccessToken(), String.class);
//		try {
//			userProfileUrl += "/" + username;
//			response = restTemplate.exchange(userProfileUrl, HttpMethod.GET, entity,
//					new ParameterizedTypeReference<MfsUserProfileResponse>() {
//			});
//		}catch (Exception e) {
//			throw new CoreServerRuntimeException(UserServiceResponseCodeConstant.UNKNOWN_EXCEPTION.getValue(),
//					UserServiceResponseCodeConstant.UNKNOWN_EXCEPTION.getDisplay(), e);
//		}
//
//		if(response == null || response.getBody() == null) {
//			throw new CoreServerRuntimeException(UserServiceResponseCodeConstant.RESPONSE_NULL);
//		}
//
//		if (!response.getBody() .getCode().equals("200")) {
//			throw new CoreServerRuntimeException(response.getBody().getCode(), response.getBody().getMessage());
//		}
//
//		return response.getBody().getData();
//	}
//
//}
@Service
public class UserServiceImpl extends BaseService implements UserService{

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserEntity userProfile(Long userId) {
		return userRepository.findById(userId).orElse(null);
	}

	@Override
	public UserEntity createUser(CreateUserRequest request) {
		return userRepository.save(modelMapper.map(request, UserEntity.class));
	}
}