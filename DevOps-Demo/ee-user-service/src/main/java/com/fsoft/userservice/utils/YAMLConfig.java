package com.fsoft.userservice.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "user-svc")
public class YAMLConfig {

	private Fhu fhu;
	private Lookup lookup;
	
	@Data
	public static class Fhu{
		private String userProfileSvc;
		private String userProfileApi;
	}

	@Data
	public static class Lookup {
		private String endPointUrl;
	}
	

}
