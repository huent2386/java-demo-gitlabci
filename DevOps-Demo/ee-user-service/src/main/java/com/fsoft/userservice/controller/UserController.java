package com.fsoft.userservice.controller;

import com.fsoft.userservice.entity.UserEntity;
import com.fsoft.userservice.logger.CreateUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fsoft.common.models.ResponseData;
import com.fsoft.userservice.domain.services.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

	@Autowired
	UserService userService;
	
	@GetMapping("/{userId}")
	public ResponseEntity<ResponseData<UserEntity>> userProfiles(@PathVariable Long userId){
		UserEntity result = userService.userProfile(userId);
		return ResponseEntity.ok(ResponseData.createSuccess(result));
	}

	@PostMapping
	public ResponseEntity<ResponseData<UserEntity>> createUser(@Valid @RequestBody CreateUserRequest request){
		UserEntity result = userService.createUser(request);
		return ResponseEntity.ok(ResponseData.createSuccess(result));
	}
}
