package com.fsoft.redis;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;


@Repository
public class RedisManagerServiceImpl implements RedisManagerService {
	private static final Logger logger = LoggerFactory.getLogger(RedisManagerServiceImpl.class);
	
	private long expireTime = 86400; // 1 day

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Override
	public void deleteKey(String key) {
		redisTemplate.delete(key);
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
	}

	@Override
	public <T> T getValue(final String key, Class<T> clzz) {
		if(!redisTemplate.hasKey(key)) {
			return null;
		}
		Object value = redisTemplate.opsForValue().get(key);
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
		return clzz.cast(value);
	}
	
	@Override
	public <T> T getValue(String groupName, final String key, Class<T> clzz) {
		if (!redisTemplate.hasKey(groupName)) {
			return null;
		}
		Object value = redisTemplate.opsForHash().get(groupName, key);
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
		
//		if(clzz == String.class ) {
//			return clzz.cast(value);
//		}
		
		return clzz.cast(value);
		
	}
	
	public static <T> T toObject(String data, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(data, clazz);
		} catch (IOException e) {
			return null;
		}
	}
	
	@Override
	public Map<Object, Object> getValue(String groupName) {
		if (!redisTemplate.hasKey(groupName)) {
			return null;
		}
		Map<Object, Object> value = redisTemplate.opsForHash().entries(groupName);
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
		
		return value;
	}
	
	@Override
	public boolean isExistedHashKey(String groupName) {
		if(CollectionUtils.isEmpty(redisTemplate.opsForHash().values(groupName))) {
			try {
				closeConnection();
			}catch(RedisConnectionFailureException e) {
				closeClients();
			}finally{
				closeConnection();
			}
			return false;
		}
		
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
		return true;
	}
	
	@Override
	public boolean isExistedUser(String username) {
		if(redisTemplate.hasKey(username)) {
			try {
				closeConnection();
			}catch(RedisConnectionFailureException e) {
				closeClients();
			}finally{
				closeConnection();
			}
			return true;
		}
		
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
		
		return false;
	}

	@Override
	public void setValue(final String key, final Object value, long timeout) {
		redisTemplate.opsForValue().set(key, value);
		redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
		
	}
	
	@Override
	public void setValue(String groupName, String key, Object value, long timeout) {
		redisTemplate.opsForHash().put(groupName, key, value);
		redisTemplate.expire(groupName, timeout, TimeUnit.SECONDS);
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
		
	}
	
	@Override
	public void setValue(String groupName, Map<Object, Object> map,long timeout) {
		redisTemplate.opsForHash().putAll(groupName, map);
		redisTemplate.expire(groupName, timeout, TimeUnit.SECONDS);
		try {
			closeConnection();
		}catch(RedisConnectionFailureException e) {
			closeClients();
		}finally{
			closeConnection();
		}
	}
	
	@Override
	public void setValue(String groupName, String key, Object value) {
		this.setValue(groupName, key, value, expireTime);
	}

	@Override
	public void setValue(final String key, final Object value) {
		
		
		this.setValue(key, value, expireTime);
	}
	
	private void closeConnection() {
		try {
			JedisConnectionFactory factory = (JedisConnectionFactory) redisTemplate.getConnectionFactory();
			factory.getConnection().close();
			factory.destroy();
		}catch (RedisConnectionFailureException e){
            logger.info("Connection closed already");
        }
		
	}
	
	private void closeClients() {
		try {
			if (null != redisTemplate.getClientList()) {
				redisTemplate.getClientList().remove(0);
				redisTemplate.getClientList().remove(1);
				redisTemplate.getClientList().forEach(redisClientInfo -> {
					String address = redisClientInfo.getAddressPort();
					if (null != address) {
						String[] addressList = address.split(":");
						redisTemplate.killClient(addressList[0], Integer.parseInt(addressList[1]));
					}
				});
			}
		} catch (Exception e) {
			logger.error("Unable to close client connections, ", e);
		}
	}
}
