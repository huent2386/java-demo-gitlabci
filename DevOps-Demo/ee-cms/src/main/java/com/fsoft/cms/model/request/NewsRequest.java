package com.fsoft.cms.model.request;

import java.util.List;

import com.fsoft.cms.entity.NewsEntity;

import lombok.Data;

@Data
public class NewsRequest {
	private List<NewsEntity> newsEntities;
}
