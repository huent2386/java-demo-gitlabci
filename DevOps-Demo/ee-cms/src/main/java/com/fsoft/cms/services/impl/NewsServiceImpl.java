package com.fsoft.cms.services.impl;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fsoft.cms.entity.CategoriesEntity;
import com.fsoft.cms.entity.NewsEntity;
import com.fsoft.cms.enums.ResponseCodeConstant;
import com.fsoft.cms.model.request.GetListNewsRequest;
import com.fsoft.cms.model.response.NewsResponse;
import com.fsoft.cms.model.response.NewsCMSResponse;
import com.fsoft.cms.model.response.NewsClientResponse;
import com.fsoft.cms.repository.CategoriesRepository;
import com.fsoft.cms.repository.NewsRepository;
import com.fsoft.cms.services.NewsService;
import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.common.models.ResponseData;

@Service
public class NewsServiceImpl implements NewsService {

	private static Logger logger = LoggerFactory.getLogger(ResponseData.class);

	@Autowired
	private NewsRepository newsRepository;

	@Autowired
	private CategoriesRepository categoriesRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<Long> insertNews(List<NewsEntity> request) {

		if (CollectionUtils.isEmpty(request)) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_EXSITS_NOT);
		} 

		StringBuilder strError = new StringBuilder();
		
		List<Long> tmp = new ArrayList<>();
		
		for (NewsEntity newsEntity : request) {
			Optional<NewsEntity> data = newsRepository.findByRefIdAndSource(
					newsEntity.getRefId(),
					newsEntity.getSource());
			
			if (data.isPresent()) {
				tmp.add(newsEntity.getRefId());
				continue;
			} 
			
			try {
				newsRepository.save(newsEntity);
			} catch (Exception e) {
				tmp.add(newsEntity.getRefId());
				strError.append(String.format("|RefId: %s, Source: %s|", 
						newsEntity.getRefId(),
						newsEntity.getSource()));
			}
		}

		String str2 = String.valueOf(strError);
		logger.info(String.format(">>>>>>> insertNews ERROR: %s", str2));

		return tmp;
	}

	@Override
	public long insertNew(NewsEntity request) {
		
		if(request.getRefId() > 0) {
			Optional<NewsEntity> data = newsRepository.findByRefIdAndSource(
					request.getRefId(),
					request.getSource());
			if (data.isPresent()) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_EXSITS);
			} 
		}
		try {
			NewsEntity entity = newsRepository.save(request);
			if(entity != null) {
				return entity.getId();
			}
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_ERROR, e);
		}
		return 0;
	}

	@Override
	public void updateNew(NewsEntity request) {

		Optional<NewsEntity> data = newsRepository.findById(request.getId());
		
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_EXSITS_NOT);
		} 
		NewsEntity entity = data.get(); 
		
		entity.setCategoryId(request.getCategoryId());
		entity.setLink(request.getLink());
		entity.setTitle(request.getTitle());
		entity.setExcerpt(request.getExcerpt());
		entity.setContent(request.getContent());
		entity.setCommentStatus(request.getCommentStatus());
		entity.setAuthor(request.getAuthor());
		entity.setImage(request.getImage());
		entity.setIsHot(request.getIsHot());
		entity.setPublishDate(request.getPublishDate());
		entity.setModifyDate(LocalDateTime.now());
		entity.setStatus(request.getStatus());
		entity.setCategoryPath(request.getCategoryPath());
		entity.setSource(request.getSource());
		
		try {
			newsRepository.save(entity);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_ERROR, e);
		}
	}

	@Override
	public void deleteNew(NewsEntity request) {

		Optional<NewsEntity> data = newsRepository.findById(request.getId());
		
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_EXSITS_NOT);
		} 
		
		try {
			newsRepository.deleteById(request.getId());
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_ERROR, e);
		}
	}
	
	@Override
	public NewsCMSResponse findByRuleForCMS(GetListNewsRequest request) {
		try {
			Sort sortable = Sort.by("modifyDate").descending();
			Pageable pageable = PageRequest.of(request.getPage() - 1, request.getPageSize(), sortable);
			Page<NewsEntity> pages = newsRepository.findByRules(request.getKeyword(), request.getIsHot(), request.getStatus(), request.getCategoryId(), request.getCreatedUser(), pageable);;

			NewsCMSResponse model = new NewsCMSResponse();
			model.setNews(pages.getContent());
			model.setTotal(pages.getTotalElements());

			return model;
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_ERROR, e);
		}
	}

	@Override
	public List<NewsResponse> findByRule(int categoryId, int page, int pageSize) {
		Sort sortable = Sort.by("modifyDate").descending();

		Pageable pageable = PageRequest.of(page - 1, pageSize, sortable);
	
		try {
			Page<NewsEntity> pages = newsRepository.findByRule(categoryId, pageable);
				
			List<NewsEntity> lstTmp = pages.getContent();
			
			Type listType = new TypeToken<List<NewsResponse>>() {}.getType();
			
			return modelMapper.map(lstTmp, listType);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_ERROR, e);
		}
	}

	@Override
	public NewsResponse findByNewId(long newId) {

		Optional<NewsEntity> data = newsRepository.findById(newId);
		
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_EXSITS_NOT);
		} 
		NewsEntity entity = data.get();
		
		return modelMapper.map(entity, NewsResponse.class);
	}
	
	@Override
	public List<NewsClientResponse> findNewsHome() {
		
		// Chỗ này cần làm (Đa luồng) Mutithead
		// ----->> Chưa làm  <<---------
		List<NewsClientResponse> lstResponse = new ArrayList<>();
		try {
			// Filter by Status = 1 and Type = 1
			List<CategoriesEntity> lst = categoriesRepository.findByRules(1, -1, 1);

			Sort sortable = Sort.by("publishDate").descending();
			
			Pageable pageable = PageRequest.of(0, 3, sortable);

			for (CategoriesEntity catEntity : lst) {
				try {	
					Page<NewsEntity> page = newsRepository.findByHomeScreen(0, catEntity.getId(), pageable);

					NewsClientResponse newsClientResponse = new NewsClientResponse();
					newsClientResponse.setCatId(catEntity.getId());
					newsClientResponse.setCatName(catEntity.getName());
					newsClientResponse.setNews(page.getContent());
					
					lstResponse.add(newsClientResponse);
				} catch (Exception e) {
					
				}
			}	
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_ERROR, e);
		}
		return lstResponse;
	}
	
	@Override
	public List<NewsResponse> findNewsHot() {
		try {
			Sort sortable = Sort.by("publishDate").descending();
			
			Pageable pageable = PageRequest.of(0, 10, sortable);
			
			Page<NewsEntity> pages = newsRepository.findByHot(pageable);

			Type listType = new TypeToken<List<NewsResponse>>() {}.getType();
		
			return modelMapper.map(pages.getContent(), listType);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.NEWS_ERROR, e);
		}
	}
}
