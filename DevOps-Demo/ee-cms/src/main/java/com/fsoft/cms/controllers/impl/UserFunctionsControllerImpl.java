package com.fsoft.cms.controllers.impl;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.UserFunctionsController;
import com.fsoft.cms.entity.UserFunctionsEntity;
import com.fsoft.cms.model.request.UserFunctionsRequest;
import com.fsoft.cms.services.UserFunctionsService;
import com.fsoft.common.models.ResponseData;

@RestController
public class UserFunctionsControllerImpl implements UserFunctionsController {

	@Autowired
	private UserFunctionsService userFunctionsService;

	@Override
	public ResponseEntity<ResponseData<List<UserFunctionsEntity>>> findAllUserFunctionsByUIdAndUName(
			@Valid @RequestParam(value = "userId", required = true) Long userId,
			@Valid @RequestParam(value = "userName", required = true) String userName) {
		
		List<UserFunctionsEntity> lst = userFunctionsService.findAllUserFunctionsByUIdAndUName(userId, userName);
		
		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}

	@Override
	public ResponseEntity<ResponseData<String>> updateUserFunctionsByUIdAndUName(
			@Valid @RequestBody UserFunctionsRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {
		
		userFunctionsService.updateUserFunctionsByUIdAndUName(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

}
