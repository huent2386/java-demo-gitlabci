package com.fsoft.cms.model.request;

import java.util.List;

import com.fsoft.cms.entity.UserFunctionsEntity;

import lombok.Data;

@Data
public class UserFunctionsRequest {
	private long userId;
	private String userName;
	private List<UserFunctionsEntity> lstUserPermission;
}
