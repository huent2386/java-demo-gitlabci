package com.fsoft.cms.controllers.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.CommentsController;
import com.fsoft.cms.entity.CommentsEntity;
import com.fsoft.cms.model.response.CommentsResponse;
import com.fsoft.cms.services.CommentsService;
import com.fsoft.common.models.ResponseData;

@RestController
public class CommentsControllerImpl implements CommentsController {

	@Autowired
	private CommentsService commentsService;
	
	@Override
	public ResponseEntity<ResponseData<List<CommentsResponse>>> findCommentByNewsId(
 			@Valid @RequestParam(value = "newsId", required = true) Long newsId,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize) {
		
		List<CommentsResponse> lst = commentsService.findCommentByNewsId(newsId, page, pageSize);
		
		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}

	@Override
	public ResponseEntity<ResponseData<List<CommentsEntity>>> findSubCommentByCommentId(
 			@Valid @RequestParam(value = "newsId", required = true) Long newsId,
 			@Valid @RequestParam(value = "commentId", required = true) Long commentId,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize) {
	
		List<CommentsEntity> lst = commentsService.findSubCommentByCommentId(newsId, commentId, page, pageSize);
		
		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}
	
	@Override
	public ResponseEntity<ResponseData<String>> addNewComment(
 			@Valid @RequestBody CommentsEntity request) {
		
		commentsService.addNewComment(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}
	
	@Override
	public ResponseEntity<ResponseData<String>> updateNewComment(
 			@Valid @RequestBody CommentsEntity request) {

		commentsService.updateNewComment(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}
}
