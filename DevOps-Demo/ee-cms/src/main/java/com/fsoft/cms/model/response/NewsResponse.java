package com.fsoft.cms.model.response;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class NewsResponse {

	private long id;
	private int categoryId;
	private long refId;
	private String link;
	private String title;
	private String excerpt;
	private String content;
	private int commentStatus;
	private String createdUser;
	private String author;
	private int commentCount;
	private int likeCount;
	private String image;
	private int isHot;
	private LocalDateTime publishDate;
	private LocalDateTime modifyDate;
	private int status;
	private String categoryPath;
	private String source;
	private int rateCount;
	private long ratePoint;
}
