package com.fsoft.cms.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fsoft.cms.entity.UserFunctionsEntity;
import com.fsoft.cms.model.request.UserFunctionsRequest;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/user/func")
public interface UserFunctionsController {

	@GetMapping("/getUserPermission")
	public ResponseEntity<ResponseData<List<UserFunctionsEntity>>> findAllUserFunctionsByUIdAndUName(
			@Valid @RequestParam(value = "userId", required = true) Long userId,
			@Valid @RequestParam(value = "userName", required = true) String userName);

	@PostMapping("/updateUserPermission")
	public ResponseEntity<ResponseData<String>> updateUserFunctionsByUIdAndUName(
			@Valid @RequestBody UserFunctionsRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);
}
