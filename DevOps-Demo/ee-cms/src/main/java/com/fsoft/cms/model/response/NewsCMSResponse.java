package com.fsoft.cms.model.response;


import java.util.List;

import com.fsoft.cms.entity.NewsEntity;

import lombok.Data;

@Data
public class NewsCMSResponse {
	private List<NewsEntity> news;
	private long total;
}
