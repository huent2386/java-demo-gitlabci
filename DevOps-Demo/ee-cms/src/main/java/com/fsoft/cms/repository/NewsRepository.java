package com.fsoft.cms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.cms.entity.NewsEntity;

@Repository
public interface NewsRepository extends JpaRepository<NewsEntity, Long> {

	@Query("SELECT n FROM NewsEntity n "
			+ "				WHERE n.refId = :refId "
			+ "					AND n.source = :source")
	public Optional<NewsEntity> findByRefIdAndSource(
			@Param("refId") long refId,
			@Param("source") String source);
	
	@Query("SELECT n FROM NewsEntity n" + 
			"				WHERE (:title = '' OR n.title LIKE %:title%)" + 
			"					AND (:isHot = -1 OR n.isHot = :isHot)" + 
			"					AND (:status = -1 OR n.status = :status)" + 
			"					AND (:categoryId = -1 OR n.categoryId = :categoryId)" + 
			"					AND (:createdUser = '' OR n.createdUser = :createdUser)")
	public Page<NewsEntity> findByRules(
			@Param("title") String title,
			@Param("isHot") int isHot,
			@Param("status") int status,
			@Param("categoryId") int categoryId,
			@Param("createdUser") String createdUser,
			Pageable pageable);

	// Client: Get List New by status, categoryId
	@Query("SELECT n FROM NewsEntity n "
			+ "				WHERE n.status = 1 "
			+ "				AND (:categoryId = -1 OR n.categoryId = :categoryId)")
	public Page<NewsEntity> findByRule(
			@Param("categoryId") int categoryId,
			Pageable pageable);

	// Client: Get List New Hot
	@Query("SELECT n FROM NewsEntity n "
			+ "				WHERE n.isHot = 1 "
			+ "					AND n.status = 1")
	public Page<NewsEntity> findByHot(Pageable pageable);
	
	// Client: Get List new by category -->> Screen Home 
	@Query("SELECT new NewsEntity("
			+ "n.id, "
			+ "n.title, "
			+ "n.likeCount, "
			+ "n.commentCount, "
			+ "n.publishDate) FROM NewsEntity n "
			+ "				WHERE (:isHot = -1 OR n.isHot = :isHot)"
			+ "				AND n.status = 1 "
			+ "				AND (:categoryId = -1 OR n.categoryId = :categoryId)")
	public Page<NewsEntity> findByHomeScreen(
			@Param("isHot") int isHot,
			@Param("categoryId") int categoryId,
			Pageable pageable);
}