package com.fsoft.cms.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@Entity
@AllArgsConstructor
@Table(name = "News")
@DynamicUpdate // Update các trường được chỉ định trong table
public class NewsEntity {
	
	public NewsEntity(long id, String title, int likeCount, int commentCount, LocalDateTime publishDate) {
		this.id = id;
		this.title = title;
		this.likeCount = likeCount;
		this.commentCount = commentCount;
		this.publishDate = publishDate;
	}
	
	public NewsEntity() {
		super();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long id;

	@Column(name = "CategoryId", nullable = false)
	private int categoryId;

	@Column(name = "RefId", nullable = false)
	private long refId;

	@Column(name = "Link", nullable = false)
	private String link;

	@Column(name = "Title", nullable = false)
	private String title;

	@Column(name = "Excerpt", nullable = false)
	private String excerpt;

	@Column(name = "Content", nullable = false)
	private String content;

	@Column(name = "CommentStatus", nullable = false)
	private int commentStatus;

	@Column(name = "CreatedUser", nullable = false)
	private String createdUser;

	@Column(name = "Author", nullable = false)
	private String author;

	@Column(name = "CommentCount", nullable = false)
	private int commentCount;

	@Column(name = "LikeCount", nullable = false)
	private int likeCount;

	@Column(name = "Image", nullable = false)
	private String image;

	@Column(name = "IsHot", nullable = false)
	private int isHot;

	@Column(name = "PublishDate", nullable = false)
	private LocalDateTime publishDate;

	@Column(name = "ModifyDate", nullable = false)
	private LocalDateTime modifyDate = LocalDateTime.now();

	@Column(name = "Status", nullable = false)
	private int status;

	@Column(name = "CategoryPath")
	private String categoryPath;

	@Column(name = "Source", nullable = false)
	private String source;

	@Column(name = "RateCount", nullable = false)
	private int rateCount;

	@Column(name = "RatePoint", nullable = false)
	private long ratePoint;
}
