package com.fsoft.cms.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.cms.entity.LikesEntity;
import com.fsoft.cms.enums.ResponseCodeConstant;
import com.fsoft.cms.model.request.ActionRequest;
import com.fsoft.cms.repository.LikesRepository;
import com.fsoft.cms.services.LikesService;
import com.fsoft.common.models.CoreServerRuntimeException;

@Service
public class LikesServiceImpl implements LikesService{

	@Autowired
	private LikesRepository likesRepostory;
	
	@Override
	public void actionPost(ActionRequest request) {
		try {
			LikesEntity entity = new LikesEntity();
			entity.setIp(request.getIp());
			entity.setItemId(request.getItemId());
			entity.setItemType(request.getItemType());
			entity.setStatus(request.getStatus());
			entity.setUserName(request.getUserName());
			likesRepostory.save(entity);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.LIKE_ERROR, e);
		}
	}
}