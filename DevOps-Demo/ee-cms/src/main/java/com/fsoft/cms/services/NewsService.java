package com.fsoft.cms.services;


import java.util.List;

import com.fsoft.cms.entity.NewsEntity;
import com.fsoft.cms.model.request.GetListNewsRequest;
import com.fsoft.cms.model.response.NewsResponse;
import com.fsoft.cms.model.response.NewsCMSResponse;
import com.fsoft.cms.model.response.NewsClientResponse;

public interface NewsService {

	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// SERVER START ///////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/**
	 * 
	 * insertNews
	 * 
	 * add NewsEntitys
	 * 
	 * @param List<NewsEntity> request
	 * @return List<NewsEntity>
	 */
	List<Long> insertNews(List<NewsEntity> request);

	/**
	 * 
	 * insertNew
	 * 
	 * add NewsEntity
	 * 
	 * @param NewsEntity request
	 */
	long insertNew(NewsEntity request);
	
	/**
	 * 
	 * updateNew
	 * 
	 * update NewsEntity
	 * 
	 * @param NewsEntity request
	 */
	void updateNew(NewsEntity request);

	/**
	 * 
	 * deleteNew
	 * 
	 * delete NewsEntity
	 * 
	 * @param NewsEntity request
	 */
	void deleteNew(NewsEntity request);
	
	/**
	 * 
	 * findByRuleForCMS
	 * 
	 * Get All NewsEntity by keyword, isHot, status, categoryid, createduser
	 * 
	 * @param GetListNewsRequest request
	 * 
	 * @return NewResponse
	 */
	NewsCMSResponse findByRuleForCMS(GetListNewsRequest request);
	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// SERVER END /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	
	
	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// CLIENT START ///////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
	/**
	 * 
	 * findByRule
	 * 
	 * Get All NewsEntity by keyword, isHot, status, categoryid, createduser
	 * 
	 * @param status
	 * @param categoryid
	 * @param createduser
	 * @param page
	 * @param pageSize
	 * 
	 * @return List<NewsResponse>
	 */
	List<NewsResponse> findByRule(int categoryId, int page, int pageSize);
	
	/**
	 * 
	 * findByRule
	 * 
	 * Get detail NewsEntity by new id
	 * 
	 * @param new id
	 * 
	 * @return NewsResponse
	 */
	NewsResponse findByNewId(long newId);

	/**
	 * 
	 * findByRule
	 * 
	 * Get All NewsEntity by Home screen
	 * 
	 * @return List<NewsClientResponse>
	 */
	List<NewsClientResponse> findNewsHome();

	/**
	 * 
	 * findNewsHot
	 * 
	 * Get All NewsEntity by Hot
	 * 
	 * @return List<NewsResponse>
	 */
	List<NewsResponse> findNewsHot();
	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// CLIENT END /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////
}
