package com.fsoft.cms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.cms.entity.UFunctionsEntity;

@Repository
public interface FunctionRepository extends JpaRepository<UFunctionsEntity, Long> {
	
	@Query("SELECT f FROM UFunctionsEntity f "
			+ "				WHERE (:fatherID = -1 OR f.fatherID = :fatherID)"
			+ "				AND (:isActive = -1 OR f.isActive = :isActive)")
	public List<UFunctionsEntity> findAllByRules(
			@Param("fatherID") int fatherID,
			@Param("isActive") int isActive);

	@Query("SELECT f FROM UFunctionsEntity f WHERE f.functionCode = :functionCode")
	public UFunctionsEntity findFunctionByFuncCode(
			@Param("functionCode") String functionCode);

	@Query("SELECT f FROM UFunctionsEntity f WHERE f.id = :id")
	public UFunctionsEntity findFunctionById(
			@Param("id") long id);
}