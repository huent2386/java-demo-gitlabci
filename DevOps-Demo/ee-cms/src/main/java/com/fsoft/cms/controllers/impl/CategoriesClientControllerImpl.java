package com.fsoft.cms.controllers.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.CategoriesClientController;
import com.fsoft.cms.entity.CategoriesEntity;
import com.fsoft.cms.services.CategoriesService;
import com.fsoft.common.models.ResponseData;

@RestController
public class CategoriesClientControllerImpl implements CategoriesClientController {

	@Autowired
	private CategoriesService categoriesService;

	@Override
	public ResponseEntity<ResponseData<List<CategoriesEntity>>> findByStatusAndType(
			@Valid @RequestParam(value = "status", required = true) Integer status,
			@Valid @RequestParam(value = "type", required = true) Integer type) {
		
		List<CategoriesEntity> lst = categoriesService.findByStatusAndStyle(status, type);

		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}

}
