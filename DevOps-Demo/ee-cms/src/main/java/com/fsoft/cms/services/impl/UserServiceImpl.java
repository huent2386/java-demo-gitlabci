package com.fsoft.cms.services.impl;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fsoft.cms.entity.UserEntity;
import com.fsoft.cms.enums.ResponseCodeConstant;
import com.fsoft.cms.model.request.UserRequest;
import com.fsoft.cms.model.response.UserResponse;
import com.fsoft.cms.repository.UserRepository;
import com.fsoft.cms.services.UserService;
import com.fsoft.common.models.CoreServerRuntimeException;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void insertUserInfo(UserEntity request) {
		CompletableFuture<Void> findByUserNameTask = CompletableFuture.runAsync(() -> {
			UserEntity	entity = userRepository.findUserByUserName(request.getUserName());
			
			if(entity != null) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS);
			}
			
		});
		
		CompletableFuture<Void> findByEmailTask = CompletableFuture.runAsync(() -> {
			UserEntity entity = userRepository.findUserByEmail(request.getEmail());
			
			if(entity != null) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_EMAIL);
			}
			
		});

		
		try {
			findByUserNameTask.join();
			findByEmailTask.join();
		}catch(Exception ex) {
			if(ex instanceof CoreServerRuntimeException) {
				throw ex;
			}
			
			throw new CoreServerRuntimeException(ResponseCodeConstant.UNKNOWN_EXCEPTION, ex);
		}
		
		try {
			userRepository.save(request);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_ERROR, e);
		}
	}

	@Override
	public void updateUserInfo(UserEntity request) {
		Optional<UserEntity> data = userRepository.findById(request.getId());
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
		}

		UserEntity entity = data.get(); 
		
		if(entity.getUserName().equals(request.getUserName())) {
		
			entity.setFullName(request.getFullName());
			entity.setEmail(request.getEmail());
			entity.setType(request.getType());
			entity.setStatus(request.getStatus());
		
			try {
				userRepository.save(entity);
			} catch (Exception e) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
			}
		}else {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
		}
	}

	@Override
	public void updateUserStatus(UserRequest request) {
		Optional<UserEntity> data = userRepository.findById(request.getUserId());
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
		}
		try {
			UserEntity entity = data.get();
			entity.setStatus(entity.getStatus() == 0 ? 1 : 0);
			userRepository.save(entity);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_ERROR, e);
		}
	}

	@Override
	public void deleteUserInfo(UserRequest request) {
		Optional<UserEntity> data = userRepository.findById(request.getUserId());
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
		}
		try {
			userRepository.deleteById(request.getUserId());
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_ERROR, e);
		}
	}

	@Override
	public void changePasswordUser(UserRequest request) {
		Optional<UserEntity> data = userRepository.findById(request.getUserId());
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
		}
		UserEntity entity = data.get();
		if (!entity.getPassword().equals(request.getOldPassword())) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_PASSWORD_NOT_MATCH);
		}
		
		if(entity.getId() == request.getUserId() && entity.getUserName().equals(request.getUserName())) {
			try {
				entity.setPassword(request.getNewPassword());
				userRepository.save(entity);
			} catch (Exception e) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT, e);
			}
		}else {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_PASSWORD_NOT_MATCH);
		}
	}
	
	@Override
	public void changePasswordUserAdmin(UserRequest request) {
		Optional<UserEntity> data = userRepository.findById(request.getUserId());
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
		}
		UserEntity entity = data.get();
		
		try {
			entity.setPassword(request.getNewPassword());
			userRepository.save(entity);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT, e);
		}
	}

	@Override
	public UserEntity findUserById(long uId) {
		Optional<UserEntity> data = userRepository.findById(uId);
		if (!data.isPresent()) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
		}
		UserEntity entity = data.get();

//		entity.setPassword(null);

		return entity;
	}

	@Override
	public UserEntity findUserByUserName(String userName) {
		try {
			UserEntity entity = userRepository.findUserByUserName(userName);
			entity.setPassword(null);
			return entity;
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT, e);
		}
	}

	@Override
	public UserResponse findAllUserByKeyword(String keyword, int status, int page, int pageSize) {
		try {
			Sort sortable = Sort.by("fullName").ascending();

			Pageable pageable = PageRequest.of(page - 1, pageSize, sortable);
			
			Page<UserEntity> pages = null;
			
			if(keyword.isEmpty() && status < 0) {
				pages = userRepository.findAllUserByKeyword(pageable);
			}else if (keyword.isEmpty()) {
				pages = userRepository.findAllUserByKeyword(status, pageable);
			}else if (status < 0) {
				pages = userRepository.findAllUserByKeyword(keyword, pageable);
			}else {
				pages = userRepository.findAllUserByKeyword(keyword, status, pageable);
			}

			UserResponse model = new UserResponse();
			model.setUsers(pages.getContent());
			model.setTotal(pages.getTotalElements());

			return model;
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT, e);
		}
	}

	@Override
	public UserEntity loginByUser(UserRequest request) {
		UserEntity entity = null;
		try {
			entity = userRepository.findUserByUserName(request.getUserName());
		} catch (Exception e) {

		} finally {
			if (entity == null) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.USER_EXSITS_NOT);
			}
			if (entity.getStatus() == 0) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.USER_ACTIVE_NOT);
			}
			if (!entity.getPassword().equals(request.getPassword())) {
				throw new CoreServerRuntimeException(ResponseCodeConstant.USER_PASSWORD_NOT_MATCH);
			}
		}
		return entity;
	}

}
