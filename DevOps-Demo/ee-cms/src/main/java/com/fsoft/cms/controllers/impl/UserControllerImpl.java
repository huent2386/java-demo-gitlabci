package com.fsoft.cms.controllers.impl;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.UserController;
import com.fsoft.cms.entity.UserEntity;
import com.fsoft.cms.model.request.UserRequest;
import com.fsoft.cms.model.response.UserResponse;
import com.fsoft.cms.services.UserService;
import com.fsoft.common.models.ResponseData;
import com.fsoft.common.utils.CommonUtils;

@RestController
public class UserControllerImpl implements UserController {

	@Autowired
	private UserService userService;

	@Override
	public ResponseEntity<ResponseData<String>> registerUser(
			@Valid @RequestBody UserEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {
		
		request.setPassword(CommonUtils.MD5(request.getPassword()));

		userService.insertUserInfo(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

	@Autowired
	HttpServletRequest httpRequest;
	
	@Override
	public ResponseEntity<ResponseData<String>> updateUserInfo(
			@Valid @RequestBody UserEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		userService.updateUserInfo(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}
	
	@Override
	public ResponseEntity<ResponseData<String>> updateUserStatus(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		userService.updateUserStatus(request);

		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}
	
	@Override
	public ResponseEntity<ResponseData<String>> deleteUserInfo(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {
		
		userService.deleteUserInfo(request);
		
		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

	@Override
	public ResponseEntity<ResponseData<String>> changePasswordUser(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		request.setOldPassword(CommonUtils.MD5(request.getOldPassword()));
		request.setNewPassword(CommonUtils.MD5(request.getNewPassword()));
		
		userService.changePasswordUser(request);

		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}
	
	@Override
	public ResponseEntity<ResponseData<String>> changePasswordUserAdmin(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName) {

		request.setNewPassword(CommonUtils.MD5(request.getNewPassword()));
		
		userService.changePasswordUserAdmin(request);

		return ResponseEntity.ok(ResponseData.createSuccess(""));
	}

	@Override
	public ResponseEntity<ResponseData<UserEntity>> findUserById(
			@Valid @RequestParam(value = "userId", required = true) long userId) {

		UserEntity userModel = userService.findUserById(userId);

		return ResponseEntity.ok(ResponseData.createSuccess(userModel));
	}

	@Override
	public ResponseEntity<ResponseData<UserEntity>> findUserByUserName(
			@Valid @RequestParam(value = "userName", required = true) String userName) {

		UserEntity userModel = userService.findUserByUserName(userName);

		return ResponseEntity.ok(ResponseData.createSuccess(userModel));
	}

	@Override
	public ResponseEntity<ResponseData<UserResponse>> findAllUserByKeyword(
			@Valid @RequestParam(value = "keyword", required = true) String keyword,
			@Valid @RequestParam(value = "status", required = true) Integer status,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize) {

		UserResponse userModel = userService.findAllUserByKeyword(keyword, status, page, pageSize);

		return ResponseEntity.ok(ResponseData.createSuccess(userModel));
	}

	@Override
	public ResponseEntity<ResponseData<UserEntity>> loginByUser(@Valid @RequestBody UserRequest request) {

		request.setPassword(CommonUtils.MD5(request.getPassword()));
		
		UserEntity userModel = userService.loginByUser(request);

		return ResponseEntity.ok(ResponseData.createSuccess(userModel));
	}

}
