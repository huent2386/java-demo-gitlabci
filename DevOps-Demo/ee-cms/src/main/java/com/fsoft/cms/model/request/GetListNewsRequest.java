package com.fsoft.cms.model.request;


import com.fsoft.common.utils.CommonUtils;

import lombok.Data;

@Data
public class GetListNewsRequest extends GetBaseRequest{
	private String keyword = "";
	private int isHot = -1;
	private int status = -1;
	private int categoryId = -1;
	private String createdUser = "";
	
	@Override
	public String toString() {
		return CommonUtils.toJsonString(this);
	}
}