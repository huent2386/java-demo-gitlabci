package com.fsoft.cms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fsoft.cms.entity.UserFunctionsEntity;

@Repository
public interface UserFunctionsRepository extends JpaRepository<UserFunctionsEntity, Long> {

	@Query("SELECT uf FROM UserFunctionsEntity uf WHERE uf.userId = :userId AND uf.userName = :userName")
	public List<UserFunctionsEntity> findAllUserFunctionsByUIdAndUName(
			@Param("userId") long userId,
			@Param("userName") String userName);

	@Modifying
	@Query("DELETE FROM UserFunctionsEntity u WHERE u.userId = :userId AND u.userName = :userName")
	public void deleteAllPermissionByUIdAndUName(
			@Param("userId") long userId, 
			@Param("userName") String userName);
}