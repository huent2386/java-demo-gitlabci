package com.fsoft.cms.controllers.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.NewsClientController;
import com.fsoft.cms.entity.NewsEntity;
import com.fsoft.cms.model.response.NewsClientResponse;
import com.fsoft.cms.model.response.NewsResponse;
import com.fsoft.cms.services.NewsService;
import com.fsoft.common.models.ResponseData;

@RestController
public class NewsClientControllerImpl implements NewsClientController {

	@Autowired
	private NewsService newsService;

	@Override
	public ResponseEntity<ResponseData<List<NewsResponse>>> findByRule(
			@Valid @RequestParam(value = "categoryId", required = true) Integer categoryId,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize) {
		
		List<NewsResponse> lst = newsService.findByRule(categoryId, page, pageSize);

		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}
	
	@Override
	public ResponseEntity<ResponseData<NewsResponse>> findNewById(
			@Valid @RequestParam(value = "newsId", required = true) Long newsId) {

		NewsResponse entity = newsService.findByNewId(newsId);

		return ResponseEntity.ok(ResponseData.createSuccess(entity));
	}

	@Override
	public ResponseEntity<ResponseData<List<NewsClientResponse>>> findNewsHome() {

		List<NewsClientResponse> lst = newsService.findNewsHome();

		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}
	
	@Override
	public ResponseEntity<ResponseData<List<NewsResponse>>> findNewsHot() {

		List<NewsResponse> lst = newsService.findNewsHot();

		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}
}
