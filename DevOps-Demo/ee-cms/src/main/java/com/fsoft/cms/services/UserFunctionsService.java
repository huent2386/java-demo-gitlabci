package com.fsoft.cms.services;

import java.util.List;

import com.fsoft.cms.entity.UserFunctionsEntity;
import com.fsoft.cms.model.request.UserFunctionsRequest;

public interface UserFunctionsService {

	/**
	 * 
	 * findAllUserFunctionsByUIdAndUName
	 * 
	 * get All User Function by User Id and User Name
	 * 
	 * @param uId
	 * @param uName
	 * @return List<UserFunctionsEntity>
	 */
	List<UserFunctionsEntity> findAllUserFunctionsByUIdAndUName(long uId, String uName);

	/**
	 * 
	 * findAllUserFunctionsByUIdAndUName
	 * 
	 * Update User Function by User Id and User Name
	 * 
	 * @param UserFunctionsRequest request
	 */
	void updateUserFunctionsByUIdAndUName(UserFunctionsRequest request);
}
