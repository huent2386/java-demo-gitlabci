package com.fsoft.cms.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Comment")
@DynamicUpdate // Update các trường được chỉ định trong table
public class CommentsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long id;

	@Column(name = "UserName", nullable = false)
	private String userName;

	@Column(name = "Ip", nullable = false)
	private String ip;

	@Column(name = "Content", nullable = false)
	private String content;

	@Column(name = "FatherId", nullable = false)
	private long fatherId = 0;

	@Column(name = "ItemId", nullable = false)
	private long itemId;

	@Column(name = "ItemType", nullable = false)
	private int itemType = 0;

	@Column(name = "Status", nullable = false)
	private int status = 1;
	
	@Column(name = "IsHaveChild", nullable = false)
	private int isHaveChild = 0;

	@Column(name = "CreatedDate", nullable = false)
	private LocalDateTime createdDate = LocalDateTime.now();

}
