package com.fsoft.cms.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fsoft.cms.entity.NewsEntity;
import com.fsoft.cms.model.request.GetListNewsRequest;
import com.fsoft.cms.model.response.NewsCMSResponse;
import com.fsoft.cms.model.response.NewsResponse;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/news/CMS")
public interface NewsCMSController {
	
	@PostMapping("/addAll")
	public ResponseEntity<ResponseData<List<Long>>> insertNews(
			@Valid @RequestBody List<NewsEntity> request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@PostMapping("/add")
	public ResponseEntity<ResponseData<Long>> insertNew(
			@Valid @RequestBody NewsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@PostMapping("/update")
	public ResponseEntity<ResponseData<String>> updateNew(
			@Valid @RequestBody NewsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);
	
	@PostMapping("/delete")
	public ResponseEntity<ResponseData<String>> deleteNew(
			@Valid @RequestBody NewsEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);
	
	@PostMapping("/getListNews")
	public ResponseEntity<ResponseData<NewsCMSResponse>> findByRule(
			@Valid @RequestBody GetListNewsRequest request);

	@GetMapping("/getDetail")
	public ResponseEntity<ResponseData<NewsResponse>> findNewById(
			@Valid @RequestParam(value = "newsId", required = true) Long newsId);
}
