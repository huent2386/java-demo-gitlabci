package com.fsoft.cms.services;

import com.fsoft.cms.entity.UserEntity;
import com.fsoft.cms.model.request.UserRequest;
import com.fsoft.cms.model.response.UserResponse;

public interface UserService {

	/**
	 * 
	 * insertUserInfo
	 * 
	 * Insert user info into DB
	 * 
	 * @param UserResponse request
	 * @return true if OK
	 */
	void insertUserInfo(UserEntity request);

	/**
	 * 
	 * updateUserInfo
	 * 
	 * Update user info into DB
	 * 
	 * @param UserResponse request
	 * @return true if OK
	 */
	void updateUserInfo(UserEntity request);

	/**
	 * 
	 * updateUserStatus
	 * 
	 * Update user status
	 * 
	 * @param UserUpdateStatusRequest request
	 * @return true if OK
	 */
	void updateUserStatus(UserRequest request);

	/**
	 * 
	 * deleteUserInfo
	 * 
	 * delete user info in DB
	 * 
	 * @param UserRequest request
	 * @return true if OK
	 */
	void deleteUserInfo(UserRequest request);

	/**
	 * 
	 * changePasswordUser
	 * 
	 * change password for user
	 * 
	 * @param uId
	 * @param uName
	 * @param curPass 		current password
	 * @param newPass 		new password
	 * @return true if OK
	 */
	void changePasswordUser(UserRequest request);

	/**
	 * 
	 * changePasswordUserAdmin
	 * 
	 * change password for user
	 * 
	 * @param uId
	 * @param uName
	 * @param newPass 		new password
	 * @return true if OK
	 */
	void changePasswordUserAdmin(UserRequest request);

	/**
	 * 
	 * findUserById
	 * 
	 * find by user id
	 * 
	 * @param uId
	 * @return UserEntity
	 */
	UserEntity findUserById(long uId);

	/**
	 * 
	 * findUserByUserName
	 * 
	 * find user by user name
	 * 
	 * @param userName
	 * @return UserEntity
	 */
	UserEntity findUserByUserName(String userName);

	/**
	 * 
	 * findAllUserByKeyword
	 * 
	 * find all user by keyword
	 * 
	 * @param keyword
	 * @param status
	 * @param page
	 * @param pageSize
	 * @return List<UserEntity>
	 */
	UserResponse findAllUserByKeyword(String keyword, int status, int page, int pageSize);
	
	/**
	 * 
	 * loginByUser
	 * 
	 * login by user
	 * 
	 * @param UserLoginRequest request
	 * @return true if OK
	 */
	UserEntity loginByUser(UserRequest request);
}
