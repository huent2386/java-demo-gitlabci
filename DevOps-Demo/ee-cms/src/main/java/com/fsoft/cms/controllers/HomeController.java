package com.fsoft.cms.controllers;

import org.springframework.web.bind.annotation.GetMapping;

public interface HomeController {

	@GetMapping("/ping")
	public String ping();
}
