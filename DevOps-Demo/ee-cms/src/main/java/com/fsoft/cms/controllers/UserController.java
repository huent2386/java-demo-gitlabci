package com.fsoft.cms.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fsoft.cms.entity.UserEntity;
import com.fsoft.cms.model.request.UserRequest;
import com.fsoft.cms.model.response.UserResponse;
import com.fsoft.common.models.ResponseData;

@RequestMapping("/user")
public interface UserController {
	
	@PostMapping("/add")
	public ResponseEntity<ResponseData<String>> registerUser(
			@Valid @RequestBody UserEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@PostMapping("/update")
	public ResponseEntity<ResponseData<String>> updateUserInfo(
			@Valid @RequestBody UserEntity request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);
	
	@PostMapping("/updateStatus")
	public ResponseEntity<ResponseData<String>> updateUserStatus(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);
	
	@PostMapping("/delete")
	public ResponseEntity<ResponseData<String>> deleteUserInfo(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@PostMapping("/changePassword")
	public ResponseEntity<ResponseData<String>> changePasswordUser(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@PostMapping("/changePasswordAdmin")
	public ResponseEntity<ResponseData<String>> changePasswordUserAdmin(
			@Valid @RequestBody UserRequest request,
			@RequestParam("curentUserId") Optional<String> curentUserId,
			@RequestParam("curentUserName") Optional<String> curentUserName);

	@GetMapping("/getByUserId")
	public ResponseEntity<ResponseData<UserEntity>> findUserById(
			@Valid @RequestParam(value = "userId", required = true) long userId);

	@GetMapping("/getByUserName")
	public ResponseEntity<ResponseData<UserEntity>> findUserByUserName(
			@Valid @RequestParam(value = "userName", required = true) String userName);

	@GetMapping("/getListUser")
	public ResponseEntity<ResponseData<UserResponse>> findAllUserByKeyword(
			@Valid @RequestParam(value = "keyword", required = true) String keyword,
			@Valid @RequestParam(value = "status", required = true) Integer status,
			@Valid @RequestParam(value = "page", required = true) Integer page,
			@Valid @RequestParam(value = "pageSize", required = true) Integer pageSize);

	@PostMapping("/login")
	public ResponseEntity<ResponseData<UserEntity>> loginByUser(
			@Valid @RequestBody UserRequest request);
}
