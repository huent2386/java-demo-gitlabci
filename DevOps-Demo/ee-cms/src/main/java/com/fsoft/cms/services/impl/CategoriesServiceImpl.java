package com.fsoft.cms.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.cms.entity.CategoriesEntity;
import com.fsoft.cms.enums.ResponseCodeConstant;
import com.fsoft.cms.repository.CategoriesRepository;
import com.fsoft.cms.services.CategoriesService;
import com.fsoft.common.models.CoreServerRuntimeException;

@Service
public class CategoriesServiceImpl implements CategoriesService {

	@Autowired
	private CategoriesRepository categoriesRepository;


	@Override
	public List<CategoriesEntity> findByStatusHotStyle(int status, int isHot, int style) {
		try {
			return categoriesRepository.findByRules(status, isHot, style);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.CATRGORIES_ERROR, e);
		}
	}
	
	@Override
	public List<CategoriesEntity> findByStatusAndStyle(int status, int style) {
		try {
			// Filter by Status and Style
			return categoriesRepository.findByRules(status, -1, style);
		} catch (Exception e) {
			throw new CoreServerRuntimeException(ResponseCodeConstant.CATRGORIES_ERROR, e);
		}
	}

}
