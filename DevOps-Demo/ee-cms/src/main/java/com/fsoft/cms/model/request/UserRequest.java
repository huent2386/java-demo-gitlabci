package com.fsoft.cms.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class UserRequest {
	private long userId;
	private String userName;
	private String oldPassword;
	private String newPassword;
	private String password;
}
