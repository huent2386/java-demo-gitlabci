package com.fsoft.cms.controllers.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.cms.controllers.CategoriesCMSController;
import com.fsoft.cms.entity.CategoriesEntity;
import com.fsoft.cms.services.CategoriesService;
import com.fsoft.common.models.ResponseData;

@RestController
public class CategoriesCMSControllerImpl implements CategoriesCMSController {

	@Autowired
	private CategoriesService categoriesService;
	
	@Override
	public ResponseEntity<ResponseData<List<CategoriesEntity>>> findByStatusHotType(
			@Valid @RequestParam(value = "status", required = true) Integer status,
			@Valid @RequestParam(value = "isHot", required = true) Integer isHot,
			@Valid @RequestParam(value = "type", required = true) Integer type) {
		
		List<CategoriesEntity> lst = categoriesService.findByStatusHotStyle(status, isHot, type);

		return ResponseEntity.ok(ResponseData.createSuccess(lst));
	}

}
