package com.fsoft.cms.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "UFunctions")
@DynamicUpdate // Update các trường được chỉ định trong table
public class UFunctionsEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "Id")
	private long id;
	
	@Column(name = "FunctionName", nullable = false)
	private String functionName;

	@Column(name = "FunctionCode", nullable = false)
	private String functionCode;

	@Column(name = "Url", nullable = false)
	private String url;

	@Column(name = "IsDisplay", nullable = false)
	private int isDisplay;

	@Column(name = "IsActive", nullable = false)
	private int isActive;

	@Column(name = "CreatedDate", nullable = false)
	private LocalDateTime createdDate = LocalDateTime.now();

	@Column(name = "FatherID", nullable = false)
	private int fatherID;
	
	@Column(name = "Priority", nullable = false)
	private int priority;
	
	@Column(name = "Icon", nullable = false)
	private String icon;
}
