package com.fsoft.cms.model.response;


import java.util.List;

import com.fsoft.cms.entity.UserEntity;

import lombok.Data;

@Data
public class UserResponse {
	private List<UserEntity> users;
	private long total;
}
