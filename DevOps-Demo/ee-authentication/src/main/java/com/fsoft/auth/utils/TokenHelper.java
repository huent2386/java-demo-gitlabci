package com.fsoft.auth.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenHelper {

    @Value("${app.name}")
    private String APP_NAME;

//    @Value("${jwt.secret}")
//    public String SECRET;

//    @Value("${jwt.expires_in}")
//    private int EXPIRES_IN;

    @Value("${jwt.mobile_expires_in}")
    private int MOBILE_EXPIRES_IN;

    @Value("${jwt.header}")
    private String AUTH_HEADER;
    
    private final String CLIENT_ID = "ClientID";

//    static final String AUDIENCE_UNKNOWN = "unknown";
//    static final String AUDIENCE_WEB = "web";
//    static final String AUDIENCE_MOBILE = "mobile";
//    static final String AUDIENCE_TABLET = "tablet";

    private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;
//
//    public String getUsernameFromToken(String token, String secret) {
//        String username;
//        try {
//            final Claims claims = this.getAllClaimsFromToken(token, secret);
//            username = claims.getSubject();
//        } catch (Exception e) {
//            username = null;
//        }
//        return username;
//    }
//
//    public Date getIssuedAtDateFromToken(String token, String secret) {
//        Date issueAt;
//        try {
//            final Claims claims = this.getAllClaimsFromToken(token, secret);
//            issueAt = claims.getIssuedAt();
//        } catch (Exception e) {
//            issueAt = null;
//        }
//        return issueAt;
//    }
//
//    public String getAudienceFromToken(String token, String secret) {
//        String audience;
//        try {
//            final Claims claims = this.getAllClaimsFromToken(token, secret);
//            audience = claims.getAudience();
//        } catch (Exception e) {
//            audience = null;
//        }
//        return audience;
//    }
//
//    public String refreshToken(String token, String secret, int expirationIn, Device device) {
//        String refreshedToken;
//        Date a = timeProvider.now();
//        try {
//            final Claims claims = this.getAllClaimsFromToken(token, secret);
//            claims.setIssuedAt(a);
//            refreshedToken = Jwts.builder()
//                .setClaims(claims)
//                .setExpiration(generateExpirationDate(device, expirationIn))
//                .signWith( SIGNATURE_ALGORITHM, secret )
//                .compact();
//        } catch (Exception e) {
//            refreshedToken = null;
//        }
//        return refreshedToken;
//    }
//
//    public String generateToken(String username, String secret, int expirationIn, Device device) {
//        String audience = generateAudience(device);
//        return Jwts.builder()
//                .setIssuer( APP_NAME )
//                .setSubject(username)
//                .setAudience(audience)
//                .setIssuedAt(timeProvider.now())
//                .setExpiration(generateExpirationDate(device, expirationIn))
//                .signWith( SIGNATURE_ALGORITHM, secret )
//                .compact();
//    }
//
//    private String generateAudience(Device device) {
//        String audience = AUDIENCE_UNKNOWN;
//        if (device.isNormal()) {
//            audience = AUDIENCE_WEB;
//        } else if (device.isTablet()) {
//            audience = AUDIENCE_TABLET;
//        } else if (device.isMobile()) {
//            audience = AUDIENCE_MOBILE;
//        }
//        return audience;
//    }
//
//    private Claims getAllClaimsFromToken(String token, String secret) {
//        Claims claims;
//        try {
//            claims = Jwts.parser()
//                    .setSigningKey(secret)
//                    .parseClaimsJws(token)
//                    .getBody();
//        } catch (Exception e) {
//            claims = null;
//        }
//        return claims;
//    }
//
//    private Date generateExpirationDate(Device device, int expirationIn) {
//        long expiresIn = device.isTablet() || device.isMobile() ? MOBILE_EXPIRES_IN : expirationIn;
//        return new Date(timeProvider.now().getTime() + expiresIn * 1000);
//    }
//
//    public int getExpiredIn(Device device, int expirationIn) {
//        return device.isMobile() || device.isTablet() ? MOBILE_EXPIRES_IN : expirationIn;
//    }
//
//    public Boolean validateToken(String token, String secret, UserDetails userDetails) {
//        UserEntity user = (UserEntity) userDetails;
//        final String username = getUsernameFromToken(token, secret);
//        final Date created = getIssuedAtDateFromToken(token, secret);
//        return (
//                username != null &&
//                username.equals(userDetails.getUsername()) &&
//                        !isCreatedBeforeLastPasswordReset(created, user.getLastPasswordResetDate())
//        );
//    }
//
//    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
//        return (lastPasswordReset != null && created.before(lastPasswordReset));
//    }

    public String getToken( HttpServletRequest request ) {
        /**
         *  Getting the token from Authentication header
         *  e.g Bearer your_token
         */
        String authHeader = request.getHeader(AUTH_HEADER);
        if ( authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }

        return null;
    }
    
    public String getClientId(HttpServletRequest request) {
    	return request.getHeader(CLIENT_ID);
    }

//    public String getAuthHeaderFromHeader( HttpServletRequest request ) {
//        return request.getHeader(AUTH_HEADER);
//    }

}