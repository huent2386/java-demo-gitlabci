package com.fsoft.auth.domain.services;

public interface LookupService {
	void findByClientIdAndSvcCode(String clientId, String svcCode);
	
	String endpointAuthenSvc(String client, String svcCode);
}
