package com.fsoft.auth.logger;

import org.aspectj.lang.annotation.Pointcut;

public class LoggingPointcut {
	@Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
	public void restcontroller() {
	}

	@Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
	public void postmapping() {
	}
	
	@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
	public void getmapping() {
	}

	@Pointcut("within(@org.springframework.stereotype.Service *)")
	public void service() {
	}

	@Pointcut("within(@org.springframework.stereotype.Component *)")
	public void component() {

	}
}