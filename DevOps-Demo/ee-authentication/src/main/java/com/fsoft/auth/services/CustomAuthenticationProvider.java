package com.fsoft.auth.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.fsoft.auth.domain.services.E2AuthenticationService;
import com.fsoft.auth.domain.services.UserRepo;
import com.fsoft.auth.models.AuthenFsoftRequest;
import com.fsoft.auth.models.MfsAuthenDTO;
import com.fsoft.redis.RedisConstants;
import com.fsoft.redis.RedisManagerService;


@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
 
	@Autowired
	E2AuthenticationService mfsAuthenService;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	RedisManagerService redisManagerService;
	
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
  
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
         
        MfsAuthenDTO userAuthen = authenticateToThirdPartySystem(name, password);
        if (userAuthen != null) {
        	List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
            grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER")); //use any GrantedAuthorities you need
            
            return new MyFsoftAuthenticationToken(userAuthen, authentication, grantedAuths, name);
        } else {
            return null;
        }
    }
 
    private MfsAuthenDTO authenticateToThirdPartySystem(String username, String password) {
    	try {
    		String redisToken = redisManagerService.getValue(username, RedisConstants.EE_TOKEN, String.class);
    		if(StringUtils.isNotEmpty(redisToken)) {
    			return MfsAuthenDTO.builder().accessToken(redisToken).build();
    		}
    		
    		MfsAuthenDTO userAuthen = mfsAuthenService.authen(new AuthenFsoftRequest(username, password, null));
    		
    		if(userAuthen != null) {
    			Map<Object, Object> mapCache = new HashMap<>();
        		mapCache.put(RedisConstants.EE_TOKEN, userAuthen.getAccessToken());
        		mapCache.put(RedisConstants.FHU_TOKEN, userAuthen.getFhuToken());
        		redisManagerService.setValue(username, mapCache, 86400);
        		redisManagerService.setValue(userAuthen.getAccessToken(), username);
    		}
    		
			return userAuthen;
    	}catch (Exception ex) {
    		throw ex;
    	}
	}

    
	@Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
          UsernamePasswordAuthenticationToken.class);
//		return MyFsoftAuthenticationToken.class.isAssignableFrom(authentication);
    }
}