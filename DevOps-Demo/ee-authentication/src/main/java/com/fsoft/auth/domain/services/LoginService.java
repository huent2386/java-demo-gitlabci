package com.fsoft.auth.domain.services;

import com.fsoft.auth.models.MfsAuthenDTO;
import com.fsoft.auth.models.request.LoginRequest;

public interface LoginService {
	MfsAuthenDTO login(LoginRequest request);
}