package com.fsoft.auth.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Builder;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class ServiceMapDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String svcCode;
	private String svcRoute;
	private String svcUrl;
	private String notes;
}
