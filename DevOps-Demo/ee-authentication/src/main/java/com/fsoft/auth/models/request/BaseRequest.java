package com.fsoft.auth.models.request;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BaseRequest {
	protected int deviceType = 4;  // 1: iOS , 2: Android, 3: Web, 4: Other
	protected String deviceToken;
	protected String manufacture;
	protected String modelName;
	protected String operatingSystem;
	protected String operatingSystemVersion;
	protected String screenResolution;
}
