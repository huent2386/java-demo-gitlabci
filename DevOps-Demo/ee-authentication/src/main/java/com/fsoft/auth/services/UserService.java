package com.fsoft.auth.services;
import com.fsoft.auth.models.UserModel;

public interface UserService {
	UserModel findUserInfoByUsername(String username);
}
