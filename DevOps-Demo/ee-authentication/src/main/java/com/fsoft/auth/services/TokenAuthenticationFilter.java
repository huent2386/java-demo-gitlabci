package com.fsoft.auth.services;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fsoft.auth.models.AuthenManagerInfo;
import com.fsoft.auth.models.UserDetailExtend;
import com.fsoft.auth.utils.TokenHelper;
import com.fsoft.redis.RedisManagerService;

public class TokenAuthenticationFilter extends OncePerRequestFilter {

	private final Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

	private TokenHelper tokenHelper;

	private CustomUserDetailsService userDetailsService;
	
	private AuthenManagerService authenManagerService;
	
	private RedisManagerService redisManagerService;

	public TokenAuthenticationFilter(TokenHelper tokenHelper, CustomUserDetailsService userDetailsService, 
			AuthenManagerService authenManagerService, RedisManagerService redisManagerService) {
		this.tokenHelper = tokenHelper;
		this.userDetailsService = userDetailsService;
		this.authenManagerService = authenManagerService;
		this.redisManagerService = redisManagerService;
	}

	@Override
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String authToken = tokenHelper.getToken(request);
		String clientId = tokenHelper.getClientId(request);

		if (authToken != null && clientId != null) {
//			AuthenManagerInfo authenInfo = authenManagerService.findByClientId(clientId);
			
//			String clientSecret = authenInfo.getClientSecret();
			String username = redisManagerService.getValue(authToken, String.class);

			if(username != null) {
//				String tokenJWT = redisManagerService.getValue(username, RedisCacheConstant.TOKEN_JWT, String.class);
//				if (tokenJWT != null) {
					// get user
					UserDetailExtend userDetails = userDetailsService.loadUserByUsername(username);
//					if (tokenHelper.validateToken(tokenJWT, clientSecret, userDetails)) {
						// create authentication
						TokenBasedAuthentication authentication = new TokenBasedAuthentication(userDetails);
						authentication.setToken(authToken);
						SecurityContextHolder.getContext().setAuthentication(authentication);
//					}
//				}
				
			}
			
		}
		chain.doFilter(request, response);
	}

}