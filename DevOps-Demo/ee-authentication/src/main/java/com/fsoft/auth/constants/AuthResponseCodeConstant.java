package com.fsoft.auth.constants;

import com.fsoft.common.models.ErrorCodeConstant;

public enum AuthResponseCodeConstant implements ErrorCodeConstant {
	EXCEPTION("AUTH-3001", ""),
	DATA_INVALID("AUTH-3002", "Data invalid (AUTH-3002)"),
	CLIENT_ID_INVALID("AUTH-3003", "ClientID invalid (AUTH-3003)"),
	TOKEN_INVALID("AUTH-3004", "Token invalid (AUTH-3004)"),
	PERMISSION_DENIED("AUTH-3005", "Permission denied (AUTH-3005)"),
	USER_NOT_FOUND("AUTH-3006", "User not found (AUTH-3006)"),
	USER_EXPIRED("AUTH-3007", "User is expired (AUTH-3007)"),
	SVC_CODE_NOT_FOUND("AUTH-3008", "Service not found (AUTH-3008)"),
	RESPONSE_NULL("AUTH-3009", "Response null (AUTH-3009)"),
	
	UNKNOWN_EXCEPTION("AUTH-4001", "System Error!!! (AUTH-4001)");

	private String value;
	private String display;
	AuthResponseCodeConstant(String value, String display) {
		this.value = value;
		this.display = display;
	}
	@Override
	public String getValue() {
		return value;
	}
	@Override
	public String getDisplay() {
		return display;
	}
	
}
