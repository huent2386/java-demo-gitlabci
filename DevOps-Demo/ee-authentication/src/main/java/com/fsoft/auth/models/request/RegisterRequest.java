package com.fsoft.auth.models.request;

import lombok.Data;

@Data
public class RegisterRequest extends BaseRequest{
	private String username;
	private String password;
}
