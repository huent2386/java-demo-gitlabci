package com.fsoft.auth.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsoft.auth.domain.services.AuthenManagerRepo;
import com.fsoft.auth.models.AuthenManagerInfo;
import com.fsoft.redis.RedisManagerService;

@Service
public class AuthenManagerServiceImpl implements AuthenManagerService{
	
	@Autowired
	RedisManagerService redisManagerService;
	
	@Autowired
	AuthenManagerRepo authenManagerRepo;
	
	@Autowired
	ModelMapper modelMapper;
	
	@Override
	public void saveCache(AuthenManagerInfo data) {
//		redisManagerService.setValue(RedisCacheConstants.AUTHEN_MANAGER, data.getClientId(), data);
	}

	@Override
	public AuthenManagerInfo findByClientId(String clientId) {
		AuthenManagerInfo data = findInCache(clientId);
		if(data != null) {
			return data;
		}
		
		AuthenManagerInfo authenEntity = authenManagerRepo.findByClientId(clientId);
		if(authenEntity != null) {
			data = modelMapper.map(authenEntity, AuthenManagerInfo.class);
			saveCache(data);
		}
		
		return data;
	}

	private AuthenManagerInfo findInCache(String clientId) {
		return null;
//		return redisManagerService.getValue(RedisCacheConstants.AUTHEN_MANAGER, clientId, AuthenManagerInfo.class);
	}
}
