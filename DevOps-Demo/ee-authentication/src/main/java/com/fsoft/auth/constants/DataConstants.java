package com.fsoft.auth.constants;

public interface DataConstants {
	public interface HeaderConst{
		public static final String AUTHORIZATION = "Authorization";
		public static final String BASIC = "Basic";
		public static final String CLIENT_ID = "ClientId";
	}
	
	public interface DeviceType {
		public static final int iOS = 1;
		public static final int Android = 2;
		public static final int Web = 3;
		public static final int Other = 4;
	}
}
