package com.fsoft.auth.domain.services;

import com.fsoft.auth.models.UserModel;

public interface UserRepo{
	UserModel findByUsername(String username);
}
