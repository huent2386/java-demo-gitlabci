package com.fsoft.auth.services;

import com.fsoft.auth.models.AuthenManagerInfo;

public interface AuthenManagerService {
	void saveCache(AuthenManagerInfo data);

	AuthenManagerInfo findByClientId(String clientId);
}
