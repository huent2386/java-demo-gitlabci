package com.fsoft.auth.controllers.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.auth.controllers.AuthenController;
import com.fsoft.auth.domain.services.LoginService;
import com.fsoft.auth.models.MfsAuthenDTO;
import com.fsoft.auth.models.request.LoginRequest;
import com.fsoft.auth.models.request.RegisterRequest;
import com.fsoft.common.models.ResponseData;

@RestController
public class AuthenControllerImpl implements AuthenController {

	@Autowired
	LoginService loginService;
	
	@Override
	public ResponseEntity<ResponseData<String>> register(@Valid @RequestBody RegisterRequest request) {
		return null;
	}

	@Override
	public ResponseEntity<ResponseData<MfsAuthenDTO>> authentication(@Valid @RequestBody LoginRequest request) {
		MfsAuthenDTO userInfo = loginService.login(request);
		return ResponseEntity.ok(ResponseData.createSuccess(userInfo));
	}

	@Override
	public ResponseEntity<ResponseData<String>> ping() {
		return ResponseEntity.ok(ResponseData.createSuccess("PONG! PONG! PONG"));
	}
	
}
