package com.fsoft.servicediscovery.constants;

import com.fsoft.common.models.ErrorCodeConstant;

public enum DiscoveryCodeConstant implements ErrorCodeConstant {
	EXCEPTION("SD-3001", ""),
	DATA_INVALID("SD-3002", "Data invalid (SD-3002)"),
	CLIENT_ID_INVALID("SD-3003", "ClientID invalid (SD-3003)"),
	TOKEN_INVALID("SD-3004", "Token invalid (SD-3004)"),
	PERMISSION_DENIED("SD-3005", "Permission denied (SD-3005)"),
	USER_NOT_FOUND("SD-3006", "User not found (SD-3006)"),
	USER_EXPIRED("SD-3007", "User is expired (SD-3007)"),
	SVC_CODE_NOT_FOUND("SD-3008", "Service not found (SD-3008)"),
	RESPONSE_NULL("SD-3009", "Response null (SD-3009)"),
	
	UNKNOWN_EXCEPTION("SD-4001", "System Error!!! (SD-4001)");

	private String value;
	private String display;
	DiscoveryCodeConstant(String value, String display) {
		this.value = value;
		this.display = display;
	}
	@Override
	public String getValue() {
		return value;
	}
	@Override
	public String getDisplay() {
		return display;
	}
	
}
