package com.fsoft.servicediscovery.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fsoft.servicediscovery.entities.TestItem;

@Repository
public interface TestItemRepos extends JpaRepository<TestItem, Long> {

	@Query(value = "insert into TestItem (nickname, user_id, username, id) values (?1, ?2, ?3, ?4))", nativeQuery = true)
	void insertData(String nickname, long userId, String username, long id);
}
