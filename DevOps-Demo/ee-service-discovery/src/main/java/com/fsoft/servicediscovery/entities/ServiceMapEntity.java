package com.fsoft.servicediscovery.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "ServiceMaps", indexes = { 
		@Index(columnList = "Id", name = "idx_servicemap_id"),
		@Index(columnList = "ClientId", name = "idx_servicemap_clientid"),
		@Index(columnList = "SvcCode", name = "idx_servicemap_svccode"),
		@Index(columnList = "CreatedTime", name = "idx_servicemap_createdtime"),
		@Index(columnList = "CreatedDate", name = "idx_servicemap_createddate")})
@DynamicUpdate
public class ServiceMapEntity {
	@Id
	@Column(name = "Id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "ClientId", length = 200, nullable = false)
	private String clientId;
	
	@Column(name = "SvcCode", length = 100, nullable = false)
	private String svcCode;

	@Column(name = "SvcRoute", length = 200, nullable = false)
	private String svcRoute;

	@Column(name = "SvcUrl", length = 200, nullable = false)
	private String svcUrl;

	@Column(name = "Notes")
	private String notes;

	@Column(name = "CreatedDate")
	@Builder.Default
	private LocalDate createdDate = LocalDate.now();

	@Column(name = "CreatedTime", nullable = false, columnDefinition = "timestamp")
	@Builder.Default
	private LocalDateTime createdTime = LocalDateTime.now();

	@Column(name = "UpdatedTime", nullable = false, columnDefinition = "timestamp")
	@Builder.Default
	private LocalDateTime updatedTime = LocalDateTime.now();

}
