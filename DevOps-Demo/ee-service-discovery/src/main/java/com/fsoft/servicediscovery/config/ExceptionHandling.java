package com.fsoft.servicediscovery.config;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.common.models.ResponseData;
import com.fsoft.common.utils.ValidationUtil;
import com.fsoft.servicediscovery.constants.DiscoveryCodeConstant;

@ControllerAdvice
public class ExceptionHandling {
	private static Logger logger = LoggerFactory.getLogger(ExceptionHandling.class);

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ResponseData<Object>> invalidInput(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<String> errors = ValidationUtil.fromBindingError(result);

		String errStr = CollectionUtils.isEmpty(errors) ? DiscoveryCodeConstant.DATA_INVALID.getDisplay() : errors.get(0);
		
		ResponseData<Object> response = ResponseData.createError(DiscoveryCodeConstant.DATA_INVALID.getValue(), errStr);

		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseData<Object>> exceptionMethod(Exception ex) {
		logger.error("====> Unknow Exception: ", ex);
		ResponseData<Object> response = ResponseData.createError(DiscoveryCodeConstant.UNKNOWN_EXCEPTION.getValue(),
				DiscoveryCodeConstant.UNKNOWN_EXCEPTION.getDisplay(), ex);

		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
	}

	@ExceptionHandler(CoreServerRuntimeException.class)
	public ResponseEntity<ResponseData<Object>> natitRuntimException(CoreServerRuntimeException ex) {
		logger.error("====> SD Exception: ",  ex);
		ResponseData<Object> response = ResponseData.createError(ex.getErrCode(), ex.getErrMsg());

		return ResponseEntity.ok(response);
	}
	
}	