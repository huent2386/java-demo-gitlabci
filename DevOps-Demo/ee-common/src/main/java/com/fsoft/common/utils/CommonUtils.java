package com.fsoft.common.utils;

import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

public class CommonUtils {
	public static <T> String toJsonString(T object) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			return "";
		}

	}

	public static <T> T toObject(String data, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(data, clazz);
		} catch (IOException e) {
			return null;
		}
	}
	
	public static <T> T xmlToObject(String data, Class<T> clazz) {
		ObjectMapper mapper = new XmlMapper();
		try {
			return mapper.readValue(data, clazz);
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String MD5(String md5) {
		return String.valueOf(DigestUtils.md5Hex(md5));
	}
}
