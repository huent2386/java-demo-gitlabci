package com.fsoft.common.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class ResponseData<T> {
	private static Logger logger = LoggerFactory.getLogger(ResponseData.class);
	public static final String SUCCESS = "200";

	private String code;
	private String message;
	private T data;

	public ResponseData() {
	}

	public ResponseData(String code, String message, T data) {
		if (message != null) {
            message = message.trim();
        }

		this.code = code;
		this.message = message;
		this.data = data;
	}

	public ResponseData(String code, String message) {
		this(code, message, null);
	}

	public static <T> ResponseData<T> createSuccess(String message, T data) {
		return new ResponseData<T>(SUCCESS, message, data);
	}

	public static <T> ResponseData<T> createSuccess(T data) {
		return createSuccess("Successfull", data);
	}

	public static <T> ResponseData<T> createError(String code, String message) {
		return new ResponseData<T>(code, message, null);
	}

	public static <T> ResponseData<T> createError(String code, String msg, Throwable ex){
		logger.error(String.format(">>> ERROR: %s - %s", code , msg), ex );
		return new ResponseData<T>(code, msg, null);
	}
	
	@JsonIgnore
	public boolean isSuccess() {
		return code  == SUCCESS;
	}
}
