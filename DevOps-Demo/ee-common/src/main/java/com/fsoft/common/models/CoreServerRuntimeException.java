package com.fsoft.common.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;

@Data
public class CoreServerRuntimeException extends RuntimeException{
	private static Logger logger = LoggerFactory.getLogger(CoreServerRuntimeException.class);
	
	private static final long serialVersionUID = 1L;

	private String errCode;

	private String errMsg;

//    public CoreServerRuntimeException(String errMsg) {
//        super();
//        this.errCode = ErrorCodeConstant.EXCEPTION.getValue();
//        this.errMsg = errMsg;
//        logger.error(">>> ERROR: " + errCode + " : " + errMsg);
//    }

	public CoreServerRuntimeException(String errCode, String errMsg) {
		super();
		this.errCode = errCode;
		this.errMsg = errMsg;
		logger.error(">>> ERROR: " + errCode + " : " + errMsg);
	}
	
	public CoreServerRuntimeException(String errCode, String errMsg, Exception ex) {
		super();
		this.errCode = errCode;
		this.errMsg = errMsg;
		logger.error(">>> ERROR: " + errCode + " : " + errMsg, ex);
	}
	
	public CoreServerRuntimeException(ErrorCodeConstant err, String notes) {
		this.errCode = err.getValue();
		this.errMsg = err.getDisplay();
		logger.error(">>> ERROR: " + errCode + " : " + errMsg + " - Note: " + notes );
	}
	
	public CoreServerRuntimeException(ErrorCodeConstant err, Throwable ex) {
		this.errCode = err.getValue();
		this.errMsg = err.getDisplay();
		logger.error(">>> ERROR: " + errCode + " : " + errMsg, ex);
	}

	public CoreServerRuntimeException(ErrorCodeConstant err, String... args) {
		this.errCode = err.getValue();
		this.errMsg = String.format(err.getDisplay(), (Object[]) args);
		logger.error(">>> ERROR: " + errCode + " : " + errMsg);
	}
}
