package com.fsoft.common.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

@SuppressWarnings("unused")
public class ValidationUtil {
	public static List<String> fromBindingError(Errors errors) {
		List<String> validErrors = new ArrayList<>();
		for (ObjectError objError : errors.getAllErrors()) {
			validErrors.add(objError.getDefaultMessage());
		}

		return validErrors;
	}

	public static String ensureNotNull(String inputString) {
	    if (inputString == null) {
	        inputString = "";
        }

        return inputString;
    }

    public static ValidationUtil init() {
	    return new ValidationUtil();
    }
}