package com.fsoft.productservice.service;

import com.fsoft.productservice.entity.ProductEntity;

import java.util.List;

public interface ProductService {
    public List<ProductEntity> findAll();

    public ProductEntity findById(Long id);

    public ProductEntity save(ProductEntity stock);

    public void deleteById(Long id);

    ProductEntity addProduct(ProductEntity request);

    ProductEntity deductProduct(ProductEntity request);
}
