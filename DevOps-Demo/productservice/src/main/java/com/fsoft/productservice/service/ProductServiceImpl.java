package com.fsoft.productservice.service;

import com.fsoft.common.models.CoreServerRuntimeException;
import com.fsoft.productservice.config.ProductServiceResponseCodeConstant;
import com.fsoft.productservice.entity.ProductEntity;
import com.fsoft.productservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<ProductEntity> findAll() {
        return productRepository.findAll();
    }

    public ProductEntity findById(Long id) {
        return productRepository.findById(id).orElse(null);
    }

    public ProductEntity save(ProductEntity stock) {
        return productRepository.save(stock);
    }

    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public ProductEntity addProduct(ProductEntity request) {
        ProductEntity product = productRepository.findByCode(request.getCode());
        if(product == null){
            return save(request);
        }

        BigDecimal val = product.getVolume().add(request.getVolume());
        if(val.compareTo(BigDecimal.ZERO) <= 0){
            throw new CoreServerRuntimeException(ProductServiceResponseCodeConstant.DATA_INVALID);
        }

        product.setVolume(val);
        return productRepository.save(product);
    }

    @Override
    public ProductEntity deductProduct(ProductEntity request) {
        ProductEntity product = productRepository.findByCode(request.getCode());
        if(product == null){
            throw new CoreServerRuntimeException(ProductServiceResponseCodeConstant.DATA_INVALID);
        }

        BigDecimal val = product.getVolume().subtract(request.getVolume());
        if(val.compareTo(BigDecimal.ZERO) < 0){
            throw new CoreServerRuntimeException(ProductServiceResponseCodeConstant.DATA_INVALID);
        }

        product.setVolume(val);
        return productRepository.save(product);
    }
}
