package com.fsoft.productservice.repository;

import com.fsoft.productservice.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    @Query("Select a from ProductEntity a where a.code = :code")
    ProductEntity findByCode(@Param("code") String code);
}
