package com.fsoft.productservice.controller;

import com.fsoft.common.models.ResponseData;
import com.fsoft.productservice.entity.ProductEntity;
import com.fsoft.productservice.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/products")
@Slf4j
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<ResponseData<List<ProductEntity>>> findAll() {
        return ResponseEntity.ok(ResponseData.createSuccess(productService.findAll()));
    }

    @PostMapping
    public ResponseEntity<ResponseData<ProductEntity>> create(@Valid @RequestBody ProductEntity product) {
        return ResponseEntity.ok(ResponseData.createSuccess(productService.addProduct(product)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<ProductEntity>> findById(@PathVariable Long id) {
        ProductEntity stock = productService.findById(id);
        if (stock == null) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(ResponseData.createSuccess(stock));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<ProductEntity>> update(@PathVariable Long id, @Valid @RequestBody ProductEntity product) {
        if (productService.findById(id) == null) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(ResponseData.createSuccess(productService.save(product)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (productService.findById(id) == null) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        productService.deleteById(id);

        return ResponseEntity.ok().build();
    }


    @PostMapping("/deduct")
    public ResponseEntity<ResponseData<ProductEntity>> deductProduct(@Valid @RequestBody ProductEntity request){
        return ResponseEntity.ok(ResponseData.createSuccess(productService.deductProduct(request)));
    }
}
