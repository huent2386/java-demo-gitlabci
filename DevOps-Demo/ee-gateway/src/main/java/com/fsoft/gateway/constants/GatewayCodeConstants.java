package com.fsoft.gateway.constants;

import com.fsoft.common.models.ErrorCodeConstant;

public enum GatewayCodeConstants implements ErrorCodeConstant{
	EXCEPTION("3001", ""),
	DATA_INVALID("3002", "Data invalid"),
	CLIENT_ID_INVALID("3003", "ClientID invalid"),
	TOKEN_INVALID("3004", "Token invalid"),
	PERMISSION_DENIED("3005", "Permission denied"),
	USER_NOT_FOUND("3006", "User not found"),
	USER_EXPIRED("3007", "User is expired"),
	SVC_CODE_NOT_FOUND("3008", "Service not found"),
	RESPONSE_NULL("3009", "Response null"),
	
	
	
	UNKNOWN_EXCEPTION("4001", "System Error!!!");

	private String value;
	private String display;
	GatewayCodeConstants(String value, String display) {
		this.value = value;
		this.display = display;
	}
	@Override
	public String getValue() {
		return value;
	}
	@Override
	public String getDisplay() {
		return display;
	}
}
