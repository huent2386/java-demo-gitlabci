package com.fsoft.gateway.config;
import java.util.LinkedList;
import java.util.List;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//解决response的body只能读一次的问题
public class RecorderServerHttpResponseDecorator extends ServerHttpResponseDecorator {
    private final List<DataBuffer> dataBuffers = new LinkedList<>();
    private boolean bufferCached = false;
    private Mono<Void> progress = null;

    public RecorderServerHttpResponseDecorator(ServerHttpResponse delegate) {
        super(delegate);
    }

//    @Override
//    public Flux<DataBuffer> getBody() {
//        synchronized (dataBuffers) {
//            if (bufferCached)
//                return copy();
//
//            if (progress == null) {
//                progress = cache();
//            }
//
//            return progress.thenMany(Flux.defer(this::copy));
//        }
//    }

    @Override
	public HttpStatus getStatusCode() {
		// TODO Auto-generated method stub
		return super.getStatusCode();
	}

	public Flux<DataBuffer> copy() {
        return Flux.fromIterable(dataBuffers)
                .map(buf -> buf.factory().wrap(buf.asByteBuffer()));
    }

//    private Mono<Void> cache() {
//        return super.getBody()
//                .map(dataBuffers::add)
//                .then(Mono.defer(()-> {
//                    bufferCached = true;
//                    progress = null;
//
//                    return Mono.empty();
//                }));
//    }
}
