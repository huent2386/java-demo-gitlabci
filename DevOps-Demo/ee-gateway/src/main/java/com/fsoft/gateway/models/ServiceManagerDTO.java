package com.fsoft.gateway.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class ServiceManagerDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String clientId;
	private String clientSecret;
	private String svcCode;
	private int expirationIn;
	private String notes;
	private int status;
}
