package com.fsoft.gateway.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class ServiceMapDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String svcCode;
	private String svcRoute;
	private String svcUrl;
	private String notes;
}
