package com.fsoft.gateway.constants;

public interface DataConstants {
	public static final String ClientID = "ClientID";

	public static final String Token = "Token";
}
