package com.fsoft.fhu.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fsoft.fhu.services.FhuCrowdServiceImpl.Password;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfoDTO {
	private String firstName;
	private String lastName;
	private String displayName;
	public String email;
	public String active;
	public String name;
}
