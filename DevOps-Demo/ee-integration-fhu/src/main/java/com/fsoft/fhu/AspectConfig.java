package com.fsoft.fhu;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "com.fsoft")
public class AspectConfig {

 public AspectConfig() {
  System.out.println("Inint Spring");
 }

}