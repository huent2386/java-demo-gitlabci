package com.fsoft.fhu.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenFsoftRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String password;
	private String sign;
	@Override
	public String toString() {
		return "AuthenFsoftRequest [username=" + username + ", sign=" + sign + "]";
	}
	
	
}
