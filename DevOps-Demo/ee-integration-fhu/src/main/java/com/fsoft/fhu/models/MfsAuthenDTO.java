package com.fsoft.fhu.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MfsAuthenDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String accessToken;
	private String username;
	private String firstName;
	private String lastName;
	private String phone;
	private String fhuToken;
}