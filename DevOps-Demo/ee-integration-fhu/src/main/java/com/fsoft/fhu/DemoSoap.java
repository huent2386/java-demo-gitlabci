package com.fsoft.fhu;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.atlassian.crowd.integration.http.CrowdHttpAuthenticator;
import com.atlassian.crowd.integration.http.CrowdHttpAuthenticatorImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelperImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractor;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractorImpl;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;

public class DemoSoap {

//	public static void main(String[] args) throws IOException {
//		try {
//			
//			final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(getProps());
//			final CrowdClient crowdClient = new RestCrowdClientFactory().newInstance(clientProperties);
//			final CrowdHttpAuthenticator crowdHttpAuthenticator = new CrowdHttpAuthenticatorImpl(
//					crowdClient, clientProperties, 
//					CrowdHttpTokenHelperImpl.getInstance(CrowdHttpValidationFactorExtractorImpl.getInstance()));
//			
//			crowdHttpAuthenticator.
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//
//	}
	
	public static Properties getProps()  {
		try {
			Properties prop = new Properties();
			try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("crowd.properties")) {
				prop.load(in);
			}
			return prop;
			
		} catch (Exception ex) {
			System.out.println(ex);
			return null;
		}
	}

}
